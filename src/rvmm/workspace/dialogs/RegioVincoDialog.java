/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.dialogs;

import static djf.AppPropertyType.NEW_WORK_TITLE;
import djf.modules.AppLanguageModule;
import djf.ui.AppNodesBuilder;
import djf.ui.dialogs.AppDialogsFacade;
import java.io.File;
import java.io.IOException;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvmm.RegioVincoMapMakerApp;
import static rvmm.workspace.style.RVMMStyle.*;
import static rvmm.RegioVincoMapMakerPropertyType.*;
import rvmm.data.RegioVincoItemPrototype;

/**
 *
 * @author HyejunJEONG
 */

/** BASICALLY THIS CLASS DOES NOTHING!! **/
public class RegioVincoDialog extends Stage {
    RegioVincoMapMakerApp app;
    public static final String DEFAULT_DIRECTORY_PROMPT = "Parent Region Directory Not Chosen";
    public static final String DEFAULT_FILE_PROMPT = "Data File Not Chosen";
    AppNodesBuilder nodesBuilder = app.getGUIModule().getNodesBuilder();

    String filePath;
    String fileName;
    File file;
    
    BorderPane newDialogPane = new BorderPane();
    GridPane gridPane;
    Pane newHeaderPane = new Pane();
    Label headerLabel = new Label();
    Label newHeaderLabel = new Label("Create New Map");
    Label regionNameLabel = new Label("Region Name");
    TextField regionNameTextField = new TextField();
    Button directoryChooserButton = new Button("Choose Parent Region Directory");
    TextArea directoryChooserText = new TextArea(DEFAULT_DIRECTORY_PROMPT);
    Button fileChooserButton = new Button("Choose Data File");
    TextArea fileChooserText = new TextArea(DEFAULT_FILE_PROMPT);
    HBox okCancelPane = new HBox();
    Button okButton = new Button("Ok");
//    Button cancelButton = new Button("Cancel");
//    
//    BorderPane editDialogPane = new BorderPane();
//    Pane editHeaderPane = new Pane();
//    Label editHeaderLabel = new Label("View/Edit Subregion");
//    HBox prevNextPane = new HBox();
//    Button prevButton = new Button();
//    Button nextButton = new Button();
//    Label subregionNameLabel = new Label("Subregion Name");
//    Label capitalLabel = new Label("Capital");
//    Label leaderNameLabel = new Label("Leader's Name");
//    Label flagLabel = new Label("Flag");
//    TextField subregionNameTextField = new TextField();
//    TextField capitalTextField = new TextField();
//    TextField leaderNameTextField = new TextField();
//    Image flagImage = new Image("Wales Flag.png");
//    
//    BorderPane mapDimensionDialogPane = new BorderPane();
//    Pane mapDimensionHeaderPane = new Pane();
//    Label mapDimensionHeaderLabel = new Label("Change Map Dimensions");
//    Label heightLabel = new Label("Height");
//    Label widthLabel = new Label("Width");
//    TextField heightTextField = new TextField();
//    TextField widthTextField = new TextField();
//    
    RegioVincoItemPrototype itemToEdit;
    RegioVincoItemPrototype newItem;
    RegioVincoItemPrototype editItem;
    boolean editing;
    
    EventHandler newItemOKHandler;
    
    public RegioVincoDialog(RegioVincoMapMakerApp initApp) {
        app = initApp;
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_RVMM_DIALOG_GRID);
        initDialog();
    
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add("workspace/style/rvmm.css");
        
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
  
    
    private void initDialog() {
        
        newHeaderLabel.setFont(Font.font("Arial", FontWeight.BOLD, 36));
        regionNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        showAndWait(); 
    }
    
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // TAKE CARE OF THE TEXT
        if (isLanguageDependent) {
            languageSettings.addLabeledControlProperty(nodeId + "_TEXT", ((Labeled)node).textProperty());
            ((Labeled)node).setTooltip(new Tooltip(""));
            languageSettings.addLabeledControlProperty(nodeId + "_TOOLTIP", ((Labeled)node).tooltipProperty().get().textProperty());
        }
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    public void showNewDialog() { // respond to new button
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(RVMM_NEW_DIALOG_HEADER_TEXT);
        newHeaderLabel.setText(headerText);
        setTitle(headerText);

        initGridNode(regionNameLabel, null, CLASS_RVMM_DIALOG_PROMPT, 0, 0, 1, 1, false);
        initGridNode(regionNameTextField, null, CLASS_RVMM_DIALOG_TEXT_FIELD, 1, 0, 1, 1, false);
        initGridNode(directoryChooserButton, null, null, 0, 1, 1, 1, false);
        initGridNode(directoryChooserText, null, null, 1, 1, 1, 1, false);
        initGridNode(fileChooserButton, null, null, 0, 2, 1, 1, false);
        initGridNode(fileChooserText, null, null, 1, 2, 1, 1, false);
        initGridNode(okButton, null, null, 1, 3, 1, 1, false);

//        regionNameTextField.setOnAction(e->{
//            processSetRegionName;
//        });
        directoryChooserButton.setOnAction(e->{
            filePath = AppDialogsFacade.showDirectoryDialog(this, NEW_WORK_TITLE);
            directoryChooserText.setText(filePath);
        });
        fileChooserButton.setOnAction(e->{
            file = AppDialogsFacade.showOpenDialog(this, NEW_WORK_TITLE);
            fileName = file.getName();
            fileChooserText.setText(fileName);

//            FileChooser fileChooser = new FileChooser();
//            fileChooser.setTitle("Create New Map");
//            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Json Files", ".json"));
//            File selectedFile = fileChooser.showOpenDialog(app.getGUIModule().getWindow());
//            if(selectedFile != null) {
//                try {
//                    app.getFileModule().loadWork(selectedFile);
//                    fileChooserText.setText(selectedFile.getName());
//
//                } catch (IOException ex) {
//                    System.out.println("[CREATE NEW MAP] load work error. File is not selected.");
//                }
//            }
        });

        Scene scene = new Scene(newDialogPane);
        this.setScene(scene);
        
        
        
        // AND OPEN THE DIALOG
        showAndWait();
    }
    
//    public void showEditDialog(RegioVincoItemPrototype initItemToEdit) { // respond to load button
//        itemToEdit = initItemToEdit;
//        
//        PropertiesManager props = PropertiesManager.getPropertiesManager();
//        String headerText = props.getProperty(RVMM_EDIT_DIALOG_HEADER_TEXT);
//        headerLabel.setText(headerText);
//        setTitle(headerText);
//
//        editHeaderLabel.setFont(Font.font("Arial", FontWeight.BOLD, 36));
//        subregionNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
//        capitalLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
//        leaderNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
//        flagLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
//    
//        prevButton = new Button();
//        nextButton = new Button();
//        app.getGUIModule().addGUINode(RVMM_DIALOG_PREVIOUS_BUTTON, prevButton);
//        app.getGUIModule().addGUINode(RVMM_DIALOG_NEXT_BUTTON, nextButton);
//        prevButton.getStyleClass().add(CLASS_RVMM_DIALOG_BUTTON);
//        nextButton.getStyleClass().add(CLASS_RVMM_DIALOG_BUTTON);
//        prevNextPane.getChildren().add(prevButton);
//        prevNextPane.getChildren().add(nextButton);
//        
//        /*RVMM_EDIT_DIALOG_HEADER,
//        RVMM_EDIT_DIALOG_HEADER_TEXT,
//        RVMM_EDIT_DIALOG_SUBREGION_NAME_LABEL,
//        RVMM_EDIT_DIALOG_SUBREGION_NAME_TEXT_FIELD,
//        RVMM_EDIT_DIALOG_CAPITAL_LABEL,
//        RVMM_EDIT_DIALOG_CAPITAL_TEXT_FIELD,
//        RVMM_EDIT_DIALOG_LEADER_NAME_LABEL,
//        RVMM_EDIT_DIALOG_LEADER_NAME_TEXT_FIELD,
//        RVMM_EDIT_DIALOG_FLAG_LABEL,
//        RVMM_EDIT_DIALOG_FLAG_IMAGE,*/
//
//        initGridNode(subregionNameLabel, RVMM_EDIT_DIALOG_SUBREGION_NAME_LABEL, CLASS_RVMM_DIALOG_PROMPT, 0, 0, 1, 1, true);
//        initGridNode(subregionNameTextField, RVMM_EDIT_DIALOG_SUBREGION_NAME_TEXT_FIELD, CLASS_RVMM_DIALOG_TEXT_FIELD, 1, 0, 1, 1, true);
//        initGridNode(capitalLabel, RVMM_EDIT_DIALOG_CAPITAL_LABEL, CLASS_RVMM_DIALOG_PROMPT, 0, 1, 1, 1, true);
//        initGridNode(capitalTextField, RVMM_EDIT_DIALOG_CAPITAL_TEXT_FIELD, CLASS_RVMM_DIALOG_TEXT_FIELD, 1, 1, 1, 1, true);
//        initGridNode(leaderNameLabel, RVMM_EDIT_DIALOG_LEADER_NAME_LABEL, CLASS_RVMM_DIALOG_PROMPT, 0, 2, 1, 1, true);
//        initGridNode(leaderNameTextField, RVMM_EDIT_DIALOG_LEADER_NAME_TEXT_FIELD, CLASS_RVMM_DIALOG_TEXT_FIELD, 1, 2, 1, 1, true);
//        initGridNode(flagLabel, RVMM_EDIT_DIALOG_FLAG_LABEL, CLASS_RVMM_DIALOG_PROMPT, 0, 3, 1, 1, true);
//        initGridNode(okCancelPane, null, CLASS_RVMM_DIALOG_PANE, 0, 4, 2, 1, false);
//        initGridNode(okButton, RVMM_DIALOG_OK_BUTTON, CLASS_RVMM_DIALOG_BUTTON, 0, 4, 1, 1, true);
////        initGridNode(flagImage, RVMM_EDIT_DIALOG_SUBREGION_NAME_TEXT_FIELD, CLASS_RVMM_DIALOG_TEXT_FIELD, 1, 0, 1, 1, true);
//
//        editing = true;
//        editItem = null;
//        
//        subregionNameTextField.setText(itemToEdit.getSubregion());
//        capitalTextField.setText(itemToEdit.getCapital());
//        leaderNameTextField.setText(itemToEdit.getLeader());
//        
//        showAndWait();
//    }
//    
//    private void processCompleteWork() {
//        String subregionName = subregionNameTextField.getText();
//        String capital = capitalTextField.getText();
//        String leaderName = leaderNameTextField.getText();
//        
//        RegioVincoData data = (RegioVincoData) app.getDataComponent();
//        if(editing) {
//            
//        }
//        
//    }
    
    
}
