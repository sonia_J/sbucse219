/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.dialogs;

import static djf.AppPropertyType.*;
import djf.modules.AppLanguageModule;
import static djf.modules.AppLanguageModule.FILE_PROTOCOL;
import djf.ui.dialogs.AppDialogsFacade;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Polygon;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvmm.RegioVincoMapMakerApp;
import static rvmm.RegioVincoMapMakerPropertyType.*;
import rvmm.data.RegioVincoData;
import rvmm.data.RegioVincoItemPrototype;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_GRID;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_HEADER;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_PANE;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_PROMPT;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_TEXT_FIELD;

/**
 *
 * @author HyejunJEONG
 */
/** THIS CLASS WORKS FOR THE ITEM DIALOG **/
public class RegioVincoItemDialog extends Stage {
    public static final String DEFAULT_SUBREGION = "Nowhere";
    public static final String DEFAULT_CAPITAL = "Nowhere";
    public static final String DEFAULT_LEADER = "No one";

    RegioVincoMapMakerApp app;
    GridPane gridPane;
    
    Label headerLabel = new Label();
    
    HBox prevNextPane = new HBox();
    Button prevButton = new Button("Previous");
    Button nextButton = new Button("Next");

    Label subregionNameLabel = new Label("Subregion Name");
    Label capitalLabel = new Label("Capital");
    Label leaderNameLabel = new Label("Leader's Name");
    Label flagLabel = new Label("Flag");
    
    TextField subregionNameTextField = new TextField();
    TextField capitalTextField = new TextField();
    TextField leaderNameTextField = new TextField();
    ImageView flagImage = new ImageView();
    
    HBox okCancelPane = new HBox();
    Button okButton =  new Button("Ok");
    Button cancelButton = new Button("Cancel");
    
    RegioVincoItemPrototype itemToEdit;
    RegioVincoItemPrototype newItem;
    RegioVincoItemPrototype editItem;
    boolean editing;

    EventHandler editItemPrevHandler;
    EventHandler editItemNextHandler;
    EventHandler editItemOkHandler;
    EventHandler editItemCancleHandler;
    
    public RegioVincoItemDialog(RegioVincoMapMakerApp initApp) {
        app = initApp;
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_RVMM_DIALOG_GRID);
        initDialog();
        
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_RVMM_DIALOG_GRID);
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        // GET THE LANGUAGE SETTINGS
        AppLanguageModule languageSettings = app.getLanguageModule();
        
        // PUT IT IN THE UI
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);

        // SETUP IT'S STYLE SHEET
        node.getStyleClass().add(styleClass);
    }
    
    String flag;
    Image appFlag;
    String imagesPath;
    PropertiesManager props;
    private void initDialog() {
        props = PropertiesManager.getPropertiesManager();
        imagesPath = props.getProperty(APP_PATH_IMAGES);
        flag = FILE_PROTOCOL + imagesPath + props.getProperty(THE_WORLD);
        appFlag = new Image(flag);
        
        flagImage.setImage(appFlag);
        flagImage.setFitWidth(200);
        flagImage.setPreserveRatio(true);
        initGridNode(headerLabel,               RVMM_EDIT_DIALOG_HEADER_TEXT,               CLASS_RVMM_DIALOG_HEADER,       0, 0, 3, 1, true);
        initGridNode(prevNextPane,              null,                                       CLASS_RVMM_DIALOG_PANE,         0, 1, 3, 1, false);
        initGridNode(subregionNameLabel,        RVMM_EDIT_DIALOG_SUBREGION_NAME_LABEL,      CLASS_RVMM_DIALOG_PROMPT,       0, 2, 1, 1, true);
        initGridNode(subregionNameTextField,    RVMM_EDIT_DIALOG_SUBREGION_NAME_TEXT_FIELD, CLASS_RVMM_DIALOG_TEXT_FIELD,   1, 2, 1, 1, true);
        initGridNode(capitalLabel,              RVMM_EDIT_DIALOG_CAPITAL_LABEL,             CLASS_RVMM_DIALOG_PROMPT,       0, 3, 1, 1, true);
        initGridNode(capitalTextField,          RVMM_EDIT_DIALOG_CAPITAL_TEXT_FIELD,        CLASS_RVMM_DIALOG_TEXT_FIELD,   1, 3, 1, 1, true);
        initGridNode(leaderNameLabel,           RVMM_EDIT_DIALOG_LEADER_NAME_LABEL,         CLASS_RVMM_DIALOG_PROMPT,       0, 4, 1, 1, true);
        initGridNode(leaderNameTextField,       RVMM_EDIT_DIALOG_LEADER_NAME_TEXT_FIELD,    CLASS_RVMM_DIALOG_TEXT_FIELD,   1, 4, 1, 1, true);
        initGridNode(flagLabel,                 RVMM_EDIT_DIALOG_FLAG_LABEL,                CLASS_RVMM_DIALOG_PROMPT,       0, 5, 1, 1, true);
        initGridNode(okCancelPane,              null,                                       CLASS_RVMM_DIALOG_PANE,         0, 6, 3, 1, false);
        gridPane.add(flagImage, 1, 5);

        okCancelPane.getChildren().addAll(okButton, cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        
        prevNextPane.getChildren().addAll(prevButton, nextButton);
        prevNextPane.setAlignment(Pos.CENTER);
        
        // AND SETUP THE EVENT HANDLERS
        subregionNameTextField.setOnAction(e->{
            processCompleteWork();
        });
        capitalTextField.setOnAction(e->{
            processCompleteWork();
        });
        leaderNameTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            newItem = null;
            editItem = null;
            this.hide();
        });  

    }
    
    private void processCompleteWork() {
        String subregionName = subregionNameTextField.getText();
        String capital = capitalTextField.getText();
        String leaderName = leaderNameTextField.getText();
        
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        if(editing) {
            if(data.isValidToDoItemEdit(itemToEdit, subregionName, capital, leaderName)) {
                itemToEdit.setSubregion(subregionName);
                itemToEdit.setCapital(capital);
                itemToEdit.setLeader(leaderName);
            }
            else {
                System.out.println("Data is not valid to edit");
            }
        }
        else {
            if(data.isValidToDoItemEdit(itemToEdit, subregionName, capital, leaderName)) {
                this.makeNewItem();
            }
            else {
                // OPEN MESSAGE DIALOG EXPLAINING WHAT WENT WRONG
                // @todo
            }        
        }
        this.hide();
    }
    
    private void makeNewItem() {
        String subregionName = subregionNameTextField.getText();
        String capital = capitalTextField.getText();
        String leaderName = leaderNameTextField.getText();
        newItem = new RegioVincoItemPrototype(subregionName, capital, leaderName);
        flag = FILE_PROTOCOL + imagesPath + "Europe/" + subregionName + " Flag.png";
        appFlag = new Image(flag);
        flagImage.setImage(appFlag);
        this.hide();
    }
    
    public RegioVincoItemPrototype getNewItem() {
        return newItem;
    }
    
    public RegioVincoItemPrototype getEditItem() {
        return editItem;
    }
    
    public void showAddDialog() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(RVMM_EDIT_DIALOG_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        
        subregionNameTextField.setText(DEFAULT_SUBREGION);
        capitalTextField.setText(DEFAULT_CAPITAL);
        leaderNameTextField.setText(DEFAULT_LEADER);
        
        flag = FILE_PROTOCOL + imagesPath + "The World.png";
        appFlag = new Image(flag);
        flagImage.setImage(appFlag);
        
        editing = false;
        editItem = null;
        
        showAndWait();
    }

    public void showEditDialog(RegioVincoItemPrototype initItemToEdit) {
        // WE'LL NEED THIS FOR VALIDATION
        itemToEdit = initItemToEdit;
        // USE THE TEXT IN THE HEADER FOR EDIT
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(RVMM_EDIT_DIALOG_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        
        // WE'LL ONLY PROCEED IF THERE IS A LINE TO EDIT
        editing = true;
        editItem = null;
        
        // USE THE TEXT IN THE HEADER FOR EDIT
        subregionNameTextField.setText(itemToEdit.getSubregion());
        capitalTextField.setText(itemToEdit.getCapital());
        leaderNameTextField.setText(itemToEdit.getLeader());
        
        flag = FILE_PROTOCOL + imagesPath + "Europe/" + itemToEdit.getSubregion() + " Flag.png";
        appFlag = new Image(flag);
        flagImage.setImage(appFlag);
        
        prevButton.setOnAction(e->{
            processPrevious(initItemToEdit);
        });
        nextButton.setOnAction(e-> {
            processNext(initItemToEdit); 
        });
        
        show();
    }
    
    public void processPrevious(RegioVincoItemPrototype currentItem) {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        int currentItemIndex = data.getItemIndex(currentItem);

        if(currentItemIndex >= 0) {
            hide();
            RegioVincoItemPrototype prevItem = data.getItemAt(currentItemIndex - 1);
            this.showEditDialog(prevItem);
            currentItemIndex = data.getItemIndex(prevItem);
        }
        else {
            AppDialogsFacade.showMessageDialog(this, "Get Previous Error", "Error: This is the First Item!");
        }
    }
    
    public void processNext(RegioVincoItemPrototype currentItem) {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        int currentItemIndex = data.getItemIndex(currentItem);
        if(currentItemIndex < (data.getNumItems())) {
            hide();
            RegioVincoItemPrototype nextItem = data.getItemAt(currentItemIndex + 1);
            this.showEditDialog(nextItem);
            currentItemIndex = data.getItemIndex(nextItem);
        }
        else {
            AppDialogsFacade.showMessageDialog(this, "Get Next Error", "Error: This is the Last Item!");
        }
    }
}
