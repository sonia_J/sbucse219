/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.dialogs;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import rvmm.RegioVincoMapMakerApp;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import static rvmm.RegioVincoMapMakerPropertyType.RVMM_MAP_SIZE_DIALOG_HEADER_TEXT;
import static rvmm.RegioVincoMapMakerPropertyType.RVMM_MAP_SIZE_DIALOG_HEIGHT_LABEL;
import static rvmm.RegioVincoMapMakerPropertyType.RVMM_MAP_SIZE_DIALOG_HEIGHT_TEXT_FIELD;
import static rvmm.RegioVincoMapMakerPropertyType.RVMM_MAP_SIZE_DIALOG_WIDTH_LABEL;
import static rvmm.RegioVincoMapMakerPropertyType.RVMM_MAP_SIZE_DIALOG_WIDTH_TEXT_FIELD;
import rvmm.data.RegioVincoData;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_GRID;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_HEADER;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_PANE;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_PROMPT;
import static rvmm.workspace.style.RVMMStyle.CLASS_RVMM_DIALOG_TEXT_FIELD;


/**
 *
 * @author HyejunJEONG
 */
public class RegioVincoMapDimensionDialog extends Stage{
    
    RegioVincoMapMakerApp app;
    GridPane gridPane;
    
    Label headerLabel = new Label();
    
    Label heightLabel = new Label("Height");
    Label widthLabel = new Label("Width");
    
    TextField heightTextField = new TextField();
    TextField widthTextField = new TextField();
    
    HBox okCancelPane = new HBox();
    Button okButton = new Button("Ok");
    Button cancelButton = new Button("Cancel");
    
    EventHandler mapDimensionOkHandler;
    
    public RegioVincoMapDimensionDialog(RegioVincoMapMakerApp initApp) {  
        app = initApp;
        gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_RVMM_DIALOG_GRID);
        initDialog();
        
        Scene scene = new Scene(gridPane);
        this.setScene(scene);
        
        app.getGUIModule().initStylesheet(this);
        scene.getStylesheets().add(CLASS_RVMM_DIALOG_GRID);
        this.initOwner(app.getGUIModule().getWindow());
        this.initModality(Modality.APPLICATION_MODAL);
    }
    
    protected void initGridNode(Node node, Object nodeId, String styleClass, int col, int row, int colSpan, int rowSpan, boolean isLanguageDependent) {
        if (col >= 0)
            gridPane.add(node, col, row, colSpan, rowSpan);
        node.getStyleClass().add(styleClass);
    }
    
    private void initDialog() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        initGridNode(headerLabel,        RVMM_MAP_SIZE_DIALOG_HEADER_TEXT,          CLASS_RVMM_DIALOG_HEADER,       0, 0, 4, 1, true);
        initGridNode(heightLabel,        RVMM_MAP_SIZE_DIALOG_HEIGHT_LABEL,         CLASS_RVMM_DIALOG_PROMPT,       0, 1, 1, 1, true);
        initGridNode(heightTextField,    RVMM_MAP_SIZE_DIALOG_HEIGHT_TEXT_FIELD,    CLASS_RVMM_DIALOG_TEXT_FIELD,   1, 1, 1, 1, true);
        initGridNode(widthLabel,         RVMM_MAP_SIZE_DIALOG_WIDTH_LABEL,          CLASS_RVMM_DIALOG_PROMPT,       0, 2, 1, 1, true);
        initGridNode(widthTextField,     RVMM_MAP_SIZE_DIALOG_WIDTH_TEXT_FIELD,     CLASS_RVMM_DIALOG_TEXT_FIELD,   1, 2, 1, 1, true);
        initGridNode(okCancelPane,       null,                                      CLASS_RVMM_DIALOG_PANE,         0, 3, 4, 1, false);
        
        okCancelPane.getChildren().addAll(okButton, cancelButton);
        okCancelPane.setAlignment(Pos.CENTER);
        
        heightTextField.setOnAction(e->{
            processCompleteWork();
        });
        widthTextField.setOnAction(e->{
            processCompleteWork();
        });
        okButton.setOnAction(e->{
            processCompleteWork();
        });
        cancelButton.setOnAction(e->{
            this.hide();
        });  
    }
    
    Pane mapPane;
    private void processCompleteWork() {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        String heightString = heightTextField.getText();
        String widthString = widthTextField.getText();
        double height = Double.parseDouble(heightString);
        double width = Double.parseDouble(widthString);
        mapPane = data.getMapPane();
        double entireHeight = app.getGUIModule().getWindow().getHeight();
        double entireWidth = app.getGUIModule().getWindow().getWidth();
        double tableHeight = entireHeight - mapPane.getHeight();
        double tableWidth = entireWidth - mapPane.getWidth();
        app.getGUIModule().getWindow().setHeight(height+tableHeight);
        app.getGUIModule().getWindow().setWidth(width+tableWidth);

//        mapPane.setLayoutX(width);
//        mapPane.setLayoutY(height);
        this.hide();
    }
    
    public void showMapDimensionDialog() {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String headerText = props.getProperty(RVMM_MAP_SIZE_DIALOG_HEADER_TEXT);
        headerLabel.setText(headerText);
        setTitle(headerText);
        mapPane = data.getMapPane();
        
        double height = mapPane.getHeight();
        double width = mapPane.getWidth();
        
        heightTextField.setText(Double.toString(height));
        widthTextField.setText(Double.toString(width));
        
        showAndWait();
    }
}
