/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.controllers;

import djf.AppTemplate;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;
import rvmm.workspace.dialogs.RegioVincoMapDimensionDialog;

/**
 *
 * @author HyejunJEONG
 */
public class MapSizeController {
    RegioVincoMapMakerApp app;
    RegioVincoMapDimensionDialog mapSizeDialog;
    
    public MapSizeController(AppTemplate initApp) {
        app = (RegioVincoMapMakerApp) initApp;
        mapSizeDialog = new RegioVincoMapDimensionDialog(app);
    }
    
    public void processResizeMap() {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        mapSizeDialog.showMapDimensionDialog();
        data.resizeMap();
        app.getFoolproofModule().updateAll();
    }
}
