/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.controllers;

import djf.AppTemplate;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */

    /** Do not use yet. EXCEPTION OCCURS */

public class MapStyleController {
    
    /** Do not use yet. EXCEPTION OCCURS */
    // THIS CLASS INCLUDES EXPANDIND MAP VIEW 
    // ALONG WITH BOTTOM BACKGROUND SETTINGS.

    RegioVincoMapMakerApp app;
    
    public MapStyleController(AppTemplate initApp) {
        app = (RegioVincoMapMakerApp) initApp;
    }
    
    public void processExpandMapView(){
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.expandMapView();
    }
//    
//    public void processChangeBackgroundGradient() {
//        RegioVincoData data = (RegioVincoData)app.getDataComponent();
//        //data.changeBackgroundGradient();
//    }
    
    public void processChangeLineThickness(double thickness) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.changeLineThickness(thickness);
    }
    
    public void processChangeLineColor(Color color) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.changeLineColor(color);
    }
    
    public void processToggleBorder(Pane pane) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.toggleBorderFrame(pane);
    }
    
    public void processRandomColor() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.reassignColor();
    }
}
