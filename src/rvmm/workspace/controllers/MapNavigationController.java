/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.controllers;

import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class MapNavigationController {
    RegioVincoMapMakerApp app;
    double moveInc = 25.0;
    
    public MapNavigationController(RegioVincoMapMakerApp initApp) {
        app = initApp;
    }

    public void processResetViewport() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.resetViewport();
    }
    
    public void processFitToRegion() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.fitToRegion();
    }
}
