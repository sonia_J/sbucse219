package rvmm.workspace.controllers;

import djf.AppTemplate;
import java.util.ArrayList;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class MapMakerController {
    RegioVincoMapMakerApp app;
    ArrayList<Double> oldDragValues;
    ArrayList<Double> newDragValues;
     
    
    public MapMakerController(AppTemplate initApp) {
        app = (RegioVincoMapMakerApp) initApp;
        oldDragValues = new ArrayList();
        newDragValues = new ArrayList();
    }
    
    public void processMapMousePress(int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.startMapDrag(x, y);
    }
    
    public void processMapMouseRelease(int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.endMapDrag(x, y);
    }
    
    public void processMapMouseClicked(boolean leftButton, int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (leftButton)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    public void processMapMouseScroll(boolean zoomIn, int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (zoomIn)
            data.zoomInOnPoint(x, y);
        else
            data.zoomOutOnPoint(x, y);
    }

    /**
     * Respond to mouse dragging on the rendering surface, which we call canvas,
     * but is actually a Pane.
     */
    public void processMapMouseMoved(int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.update(x, y);
    }
    

    public void processMapMouseDragged(double x, double y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.updateMapDrag(x, y);
    }
}
