/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.controllers;

import djf.AppTemplate;
import java.util.HashMap;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Polygon;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;
import rvmm.data.RegioVincoItemPrototype;
import rvmm.transactions.AddItemTransaction;
import rvmm.transactions.EditItemTransaction;
import rvmm.transactions.MoveItemTransaction;
import rvmm.workspace.dialogs.RegioVincoDialog;
import rvmm.workspace.dialogs.RegioVincoItemDialog;

/**
 *
 * @author HyejunJEONG
 */
public class ItemsController {
    RegioVincoMapMakerApp app;
    RegioVincoDialog dialog;
    RegioVincoItemDialog itemDialog;
    HashMap<Polygon, RegioVincoItemPrototype> polygonItemMap;

    
    public ItemsController(AppTemplate initApp) {
        app = (RegioVincoMapMakerApp) initApp;
        itemDialog = new RegioVincoItemDialog(app);
        polygonItemMap = new HashMap<>();
    }
    
    public void processRenameMap() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.renameMap();
        app.getFoolproofModule().updateAll();
    }
// This method works. 
//    public void processHighlightItemMatching(Polygon poly) {
    public void processHighlightItemMatching(Polygon poly) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if(data.getPolyItemMap().containsKey(poly)) {
            RegioVincoItemPrototype item = data.getItem(poly);
            if(item != null) {
                System.out.println("processHighlightSelectedItem: " + item);
                data.highlightItem(item);
            }
        }
    }
    
    // When Item is selected, highlight poly
    public void processHighlightPolyMatching(RegioVincoItemPrototype item) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        Polygon poly = data.getPolygon(item);
        if(poly != null) {
            data.highlightPolygon(poly);
        }
    }
    
    public void processMoveItemUp() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex > 0) {
                MoveItemTransaction transaction = new MoveItemTransaction(data, oldIndex, oldIndex-1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
    public void processMoveItemDown() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToMove = data.getSelectedItem();
            int oldIndex = data.getItemIndex(itemToMove);
            if (oldIndex < (data.getNumItems()-1)) {
                MoveItemTransaction transaction = new MoveItemTransaction(data, oldIndex, oldIndex+1);
                app.processTransaction(transaction);
                
                // DESELECT THE OLD INDEX
                data.clearSelected();
                
                // AND SELECT THE MOVED ONE
                data.selectItem(itemToMove);
                
                // UPDATE BUTTONS
                app.getFoolproofModule().updateAll();
            }
        }
    }
    
//    public void processAddItem(File workFile) {
    public void processAddItem(double mouseX, double mouseY){
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        Polygon poly = data.getMousedOverPolygon(mouseX, mouseY);
        if(poly != null) {
            itemDialog.showAddDialog();
            RegioVincoItemPrototype newItem = itemDialog.getNewItem();
                if(newItem != null) {
                    data.addItem(newItem, mouseX, mouseY);
                    //    AddItemTransaction transaction = new AddItemTransaction(data, newItem);
                      //  app.processTransaction(transaction);
                }
                else {
                    djf.ui.dialogs.AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), "Invalid Line", "Invalid data for a new line");
                }
//                }
//            });
        }
    }
    
    
    public void processMapClicked(Pane map) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
            map.setOnMouseClicked(e->{
            Polygon poly = data.getMousedOverPolygon(e.getX(), e.getY());
            if(e.getClickCount() == 1) {
                //poly = data.getMousedOverPolygon(e.getX(), e.getY());
                processHighlightItemMatching(poly);
            }
            // uncomment below: no dialog shown.
            else if(e.getClickCount() == 2) {
                System.out.println("double clicked");

                if(data.getPolyItemMap().containsKey(poly)) {
                    System.out.println("containskey");
                    processEditItem();
                }
                else {
                    processAddItem(e.getX(), e.getY());
                }
            }
            app.getFoolproofModule().updateAll();
        });
    }
    
    public void processEditItem() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        if (data.isItemSelected()) {
            RegioVincoItemPrototype itemToEdit = data.getSelectedItem();
            itemDialog.showEditDialog(itemToEdit);
            RegioVincoItemPrototype editItem = itemDialog.getEditItem();
            if (editItem != null) {
//                EditItemTransaction transaction = new EditItemTransaction(itemToEdit, editItem.getSubregion(), editItem.getCapital(), editItem.getLeader());
//                app.processTransaction(transaction);
                itemToEdit.setSubregion(editItem.getSubregion());
                itemToEdit.setCapital(editItem.getCapital());
                itemToEdit.setLeader(editItem.getLeader());
                polygonItemMap.replace(data.getPolygon(itemToEdit), itemToEdit, editItem);
            }
        }
    }    
    
}
