/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.workspace.controllers;

import djf.AppTemplate;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class ImageController {
    RegioVincoMapMakerApp app;
    ArrayList<Double> oldDragValues;
    ArrayList<Double> newDragValues;
    
    public ImageController(AppTemplate initApp) {
        app = (RegioVincoMapMakerApp) initApp;
        oldDragValues = new ArrayList();
        newDragValues = new ArrayList();
    }
    
    /* moveImage helper methods */
    public void processImageMousePress(int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.startImageDrag(x, y);
    }
    public void processImageMouseDragged(double x, double y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.updateImageDrag(x, y);
    }
    public void processImageMouseRelease(int x, int y) {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.endImageDrag(x, y);
    }    
    public void processAddImage(){
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.addImage();
        app.getFoolproofModule().updateAll();
    }
    
    public void processRemoveImage() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
        data.removeImage();
        app.getFoolproofModule().updateAll();
    }
}
