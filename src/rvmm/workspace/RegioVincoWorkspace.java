package rvmm.workspace;

import static djf.AppPropertyType.EXPORT_BUTTON;
import static djf.AppPropertyType.NEW_BUTTON;
import static djf.AppPropertyType.LOAD_BUTTON;
import static djf.AppPropertyType.SAVE_BUTTON;
import static djf.AppPropertyType.EXIT_BUTTON;
import static djf.AppPropertyType.NEW_WORK_TITLE;
import djf.components.AppWorkspaceComponent;
import djf.modules.AppFoolproofModule;
import djf.modules.AppGUIModule;
import static djf.modules.AppGUIModule.ENABLED;
import djf.ui.AppNodesBuilder;
import static djf.modules.AppGUIModule.FOCUS_TRAVERSABLE;
import static djf.modules.AppGUIModule.HAS_KEY_HANDLER;
import djf.ui.dialogs.AppDialogsFacade;
import static djf.ui.style.DJFStyle.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.Modality;
import properties_manager.PropertiesManager;

import rvmm.RegioVincoMapMakerApp;
import rvmm.workspace.foolproof.RegioVincoSelectionFoolproofDesign;
import static rvmm.RegioVincoMapMakerPropertyType.*;
import rvmm.data.RegioVincoData;
import rvmm.data.RegioVincoItemPrototype;
import rvmm.workspace.controllers.ImageController;
import rvmm.workspace.controllers.ItemsController;
import rvmm.workspace.controllers.ItemsTableController;
import rvmm.workspace.controllers.MapMakerController;
import rvmm.workspace.controllers.MapNavigationController;
import rvmm.workspace.controllers.MapSizeController;
import rvmm.workspace.controllers.MapStyleController;
import rvmm.workspace.dialogs.RegioVincoMapDimensionDialog;
import static rvmm.workspace.style.RVMMStyle.*;

/**
 *
 * @author Hyejun Jeong
 */
public class RegioVincoWorkspace extends AppWorkspaceComponent {

    GridPane backgroundGradientPane;
    public static final String DEFAULT_DIRECTORY_PROMPT = "Parent Region Directory Not Chosen";
    public static final String DEFAULT_FILE_PROMPT = "Data File Not Chosen";
    private boolean mouseDragOnDivider = false;
    ColorPicker borderColorPicker;
    Slider borderThicknessSlider;
    MapStyleController styleController;
    MapSizeController sizeController;
    ImageController imageController;
    ItemsController itemsController;
    ItemsTableController tableController;
    
    Button snapTopLeftButton;
    Button snapBottomLeftButton;
    
    public RegioVincoWorkspace(RegioVincoMapMakerApp app) {
        super(app);
        initLayout();
        initFoolproofDesign();
    }

    Pane mapPane;
    TableView<RegioVincoItemPrototype> itemsTable;
    private void initLayout() {
        // load the font families for the combo box
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppNodesBuilder workspaceBuilder = app.getGUIModule().getNodesBuilder();
        itemsController = new ItemsController(app);
        styleController = new MapStyleController(app);
        sizeController = new MapSizeController(app);

        mapPane = new Pane();
        
        // clip the map so we can zoom
        BorderPane outerPane = new BorderPane();
        Rectangle clippingRectangle = new Rectangle();
        outerPane.setClip(clippingRectangle);
        
        SplitPane mapItemPane = new SplitPane();
        outerPane.setCenter(mapItemPane);
        
        Rectangle ocean = new Rectangle();
        mapPane.getChildren().add(ocean);
        //ocean.getStyleClass().add(CLASS_RVMM_MAP_OCEAN);
        
        RadialGradient rGradient = new RadialGradient(0, 0, 0, 0, 750, false, CycleMethod.NO_CYCLE, new Stop(0, Color.web("#9999FF")), new Stop(1, Color.MIDNIGHTBLUE));
        ocean.setFill(rGradient);
        app.getGUIModule().addGUINode(RVMM_MAP_PANE, mapPane);

//        mapPane.minWidthProperty().bind(outerPane.widthProperty());
//        mapPane.maxWidthProperty().bind(outerPane.widthProperty());
//        mapPane.minHeightProperty().bind(outerPane.heightProperty());
//        mapPane.maxHeightProperty().bind(outerPane.heightProperty());
        outerPane.layoutBoundsProperty().addListener((ov, oldValue, newValue) -> {
            clippingRectangle.setWidth(newValue.getWidth());
            clippingRectangle.setHeight(newValue.getHeight());
            ocean.setWidth(newValue.getHeight()*2);
            ocean.setHeight(newValue.getHeight());
        });
        
        // adding new toolbars after the existing toolbar
        HBox topToolbarPane = workspaceBuilder.buildHBox(RVMM_PANE, null, null, CLASS_RVMM_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        HBox fileButtonsPane = workspaceBuilder.buildHBox(RVMM_PANE, topToolbarPane, null, CLASS_DJF_TOOLBAR_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button newButton = workspaceBuilder.buildIconButton(NEW_BUTTON, fileButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button loadButton = workspaceBuilder.buildIconButton(LOAD_BUTTON, fileButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button saveButton = workspaceBuilder.buildIconButton(SAVE_BUTTON, fileButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button exportButton = workspaceBuilder.buildIconButton(EXPORT_BUTTON, fileButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button exitButton = workspaceBuilder.buildIconButton(EXIT_BUTTON, fileButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        HBox polygonButtonsPane = workspaceBuilder.buildHBox(RVMM_POLYGON_TOOLBAR_PANE, topToolbarPane, null, CLASS_DJF_TOOLBAR_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button fitToRegionButton = workspaceBuilder.buildIconButton(RVMM_FIT_VIEWPORT_BUTTON, polygonButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button resetViewportButton = workspaceBuilder.buildIconButton(RVMM_RESET_VIEWPORT_BUTTON, polygonButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        polygonButtonsPane.setPadding(new Insets(6, 6, 6, 7));
        polygonButtonsPane.setSpacing(5);
        
        HBox imageButtonsPane = workspaceBuilder.buildHBox(RVMM_IMAGE_TOOLBAR_PANE, topToolbarPane, null, CLASS_DJF_TOOLBAR_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button changeMapNameButton = workspaceBuilder.buildIconButton(RVMM_CHANGE_MAP_NAME_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button addImageButton = workspaceBuilder.buildIconButton(RVMM_ADD_IMAGE_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button removeImageButton = workspaceBuilder.buildIconButton(RVMM_REMOVE_IMAGE_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        snapTopLeftButton = workspaceBuilder.buildIconButton(RVMM_SNAP_TOP_LEFT_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        snapBottomLeftButton = workspaceBuilder.buildIconButton(RVMM_SNAP_BOTTOM_LEFT_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button changeMapColorsButton = workspaceBuilder.buildIconButton(RVMM_CHANGE_MAP_COLORS_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button changeMapSizeButton = workspaceBuilder.buildIconButton(RVMM_CHANGE_MAP_SIZE_BUTTON, imageButtonsPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        imageButtonsPane.setPadding(new Insets(6, 6, 6, 7));
        imageButtonsPane.setSpacing(5);
        
        /** ADD/REMOVE/MOVE IMAGE **/
        imageController = new ImageController(app);
        addImageButton.setOnAction(e->{
            imageController.processAddImage();
            app.getFileModule().markAsEdited(true);
        });
        removeImageButton.setOnAction(e->{
            imageController.processRemoveImage();
            app.getFileModule().markAsEdited(true);
        });
        
        changeMapSizeButton.setOnAction(e-> {
            sizeController.processResizeMap();
        });
        
        changeMapColorsButton.setOnAction(e->{
            styleController.processRandomColor();
        });
        
        HBox toggleButtonsPane = workspaceBuilder.buildHBox(RVMM_TOGGLES_TOOLBAR_PANE, topToolbarPane, null, CLASS_DJF_TOOLBAR_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Label mapFrameLabel = new Label("Toggle Frame");
        mapFrameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        toggleButtonsPane.getChildren().add(mapFrameLabel);
        CheckBox mapFrameButton = workspaceBuilder.buildCheckBox(RVMM_TOGGLES_MAP_FRAME_CHECK_BOX, toggleButtonsPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);   
        mapFrameButton.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                //styleController.processToggleBorder(mapPane);
                if(newValue) {
                    ocean.setStroke(Color.BLACK);
                    ocean.setStrokeWidth(10);
                }
                else {
                    ocean.setStroke(Color.TRANSPARENT);
                }
            }
        });
        
        Label changeSubregionLabel = new Label("Move Polygon Mode");
        changeSubregionLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        toggleButtonsPane.getChildren().add(changeSubregionLabel);

        CheckBox changeSubregionButton = workspaceBuilder.buildCheckBox(RVMM_TOGGLES_CHANGE_SUBREGION_CHECK_BOX, toggleButtonsPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        toggleButtonsPane.setPadding(new Insets(10, 6, 6, 7));
        toggleButtonsPane.setSpacing(5);
        
        HBox borderStylePane = workspaceBuilder.buildHBox(RVMM_BORDER_TOOLBAR_PANE, topToolbarPane, null, CLASS_DJF_TOOLBAR_PANE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        Label borderColorLabel = new Label("Border Color");
        borderColorLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        borderStylePane.getChildren().add(borderColorLabel);        
        borderColorPicker = workspaceBuilder.buildColorPicker(RVMM_BORDER_COLOR_PICKER, borderStylePane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);   
        borderColorPicker.setOnAction(e-> {
            styleController.processChangeLineColor(borderColorPicker.getValue());
        });
        
        Label borderThicknessLabel = new Label("Border Thickness");
        borderThicknessLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        borderStylePane.getChildren().add(borderThicknessLabel);
        borderThicknessSlider = workspaceBuilder.buildSlider(RVMM_BORDER_THICKNESS_SLIDER, borderStylePane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 4);
        borderStylePane.setPadding(new Insets(10, 6, 6, 7));
        borderStylePane.setSpacing(5);
        
        borderThicknessSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                styleController.processChangeLineThickness((double) newValue);
            }            
        });

        
        VBox itemsPane       = workspaceBuilder.buildVBox(RVMM_ITEMS_PANE, null, null, CLASS_RVMM_BOX, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        HBox itemsHeaderPane = workspaceBuilder.buildHBox(RVMM_ITEMS_HEADER_PANE, itemsPane, null, CLASS_RVMM_BIG_HEADER, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        HBox itemsHeaderLabelButtonsPane = workspaceBuilder.buildHBox(RVMM_ITEMS_HEADER_PANE, itemsHeaderPane, null, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label itemsHeaderLabel = new Label("The World");
        itemsHeaderLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        Button moveUpButton   = workspaceBuilder.buildIconButton(RVMM_MOVE_UP_BUTTON,   itemsHeaderPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        Button moveDownButton = workspaceBuilder.buildIconButton(RVMM_MOVE_DOWN_BUTTON, itemsHeaderPane, null, CLASS_DJF_ICON_BUTTON, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        itemsHeaderLabelButtonsPane.getChildren().addAll(itemsHeaderLabel, moveUpButton, moveDownButton);

        itemsHeaderLabelButtonsPane.setAlignment(Pos.CENTER);
        
        // AND NOW THE TABLE
        itemsTable  = workspaceBuilder.buildTableView(RVMM_ITEMS_TABLE_VIEW, itemsPane, null, CLASS_RVMM_TABLE, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE,  true);
        TableColumn<RegioVincoItemPrototype, String> subregionsColumn = workspaceBuilder.buildTableColumn(RVMM_SUBREGION_COLUMN, itemsTable, CLASS_RVMM_COLUMN);
        TableColumn<RegioVincoItemPrototype, String> capitalColumn    = workspaceBuilder.buildTableColumn(RVMM_CAPITAL_COLUMN,   itemsTable, CLASS_RVMM_COLUMN);
        TableColumn<RegioVincoItemPrototype, String> leaderColumn     = workspaceBuilder.buildTableColumn(RVMM_LEADER_COLUMN,    itemsTable, CLASS_RVMM_COLUMN);
        //itemsTable.setMinHeight(480);
        itemsTable.getColumnResizePolicy();
        
        itemsTable.getColumns().get(0).prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.33));
        itemsTable.getColumns().get(1).prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.33));
        itemsTable.getColumns().get(2).prefWidthProperty().bind(itemsTable.widthProperty().multiply(0.33));
        
        RegioVincoItemPrototype prototype = new RegioVincoItemPrototype("Somewhere", "Nowhere", "No one");
        
        // SPECIFY THE TYPES FOR THE COLUMNS
        subregionsColumn.setCellValueFactory(new PropertyValueFactory<>("subregion"));
        capitalColumn.setCellValueFactory(   new PropertyValueFactory<>("capital"));
        leaderColumn.setCellValueFactory(    new PropertyValueFactory<>("leader"));

        tableController = new ItemsTableController(app);
        itemsTable.widthProperty().addListener(e->{
            tableController.processChangeTableSize();
        });
        
        backgroundGradientPane = new GridPane();
        backgroundGradientPane.setPadding(new Insets(5));
        backgroundGradientPane.setVgap(5); // or 8
        backgroundGradientPane.setHgap(10);
        itemsPane.getChildren().add(backgroundGradientPane);
        
        /** BACKGROUND GRADIENT PANE **/
        Label headerLabel = new Label("Background Gradient");
        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(headerLabel);
        backgroundGradientPane.setConstraints(headerLabel, 0, 0, 2, 1);

        Label proportionalLabel = new Label("Proportional");
        proportionalLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(proportionalLabel);
        backgroundGradientPane.setConstraints(proportionalLabel, 3, 0, 1, 1);
        CheckBox proportionalCheckBox = workspaceBuilder.buildCheckBox(RVMM_BACKGROUND_PROPORTIONAL_CHECK_BOX, backgroundGradientPane, 4, 0, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        Label focusAngleLabel = new Label("Focus Angle");
        focusAngleLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(focusAngleLabel);
        backgroundGradientPane.setConstraints(focusAngleLabel, 0, 1, 1, 1);
        Slider focusAngleSlider = workspaceBuilder.buildSlider(RVMM_BACKGROUND_FOCUS_ANGLE_SLIDER, backgroundGradientPane, 1, 1, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 360);
        
        Label focusDistanceLabel = new Label("Focus Distance");
        focusDistanceLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(focusDistanceLabel);
        backgroundGradientPane.setConstraints(focusDistanceLabel, 3, 1, 1, 1);
        Slider focusDistanceSlider = workspaceBuilder.buildSlider(RVMM_BACKGROUND_FOCUS_DISTANCE_SLIDER, backgroundGradientPane, 4, 1, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 1000);

        Label centerXLabel = new Label("Center X");
        centerXLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(centerXLabel);
        backgroundGradientPane.setConstraints(centerXLabel, 0, 2, 1, 1);
        Slider centerXSlider = workspaceBuilder.buildSlider(RVMM_BACKGROUND_CENTER_X_SLIDER, backgroundGradientPane, 1, 2, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 1000);

        Label centerYLabel = new Label("Center Y");
        centerYLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(centerYLabel);
        backgroundGradientPane.setConstraints(centerYLabel, 3, 2, 1, 1);
        Slider centerYSlider = workspaceBuilder.buildSlider(RVMM_BACKGROUND_CENTER_Y_SLIDER, backgroundGradientPane, 4, 2, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 1000);

        Label radiusLabel = new Label("Radius");
        radiusLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(radiusLabel);
        backgroundGradientPane.setConstraints(radiusLabel, 0, 3, 1, 1);
        Slider radiusSlider = workspaceBuilder.buildSlider(RVMM_BACKGROUND_RADIUS_SLIDER, backgroundGradientPane, 1, 3, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED, 0, 2000);

        Label cycleMethodLabel = new Label("Cycle Method");
        cycleMethodLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(cycleMethodLabel);
        backgroundGradientPane.setConstraints(cycleMethodLabel, 3, 3, 1, 1);
        ObservableList<CycleMethod> cycleMethodList = FXCollections.observableArrayList(CycleMethod.values());
        ComboBox<CycleMethod> cycleMethodComboBox = new ComboBox(cycleMethodList);
        backgroundGradientPane.getChildren().add(cycleMethodComboBox);
        backgroundGradientPane.setConstraints(cycleMethodComboBox, 4, 3, 2, 1);

        Label stop0Label = new Label("Stop 0 Color");
        stop0Label.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(stop0Label);
        backgroundGradientPane.setConstraints(stop0Label, 0, 4, 1, 1);
        ColorPicker stop0ColorPicker = workspaceBuilder.buildColorPicker(RVMM_BACKGROUND_STOP_0_PICKER, backgroundGradientPane, 1, 4, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);

        Label stop1Label = new Label("Stop 1 Color");
        stop1Label.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        backgroundGradientPane.getChildren().add(stop1Label);
        backgroundGradientPane.setConstraints(stop1Label, 3, 4, 1, 1);
        ColorPicker stop1ColorPicker = workspaceBuilder.buildColorPicker(RVMM_BACKGROUND_STOP_1_PICKER, backgroundGradientPane, 4, 4, 2, 1, null, HAS_KEY_HANDLER, FOCUS_TRAVERSABLE, ENABLED);
        
        mapItemPane.getItems().addAll(mapPane, itemsPane);
        mapItemPane.setDividerPositions(0.5f);

        mapItemPane.getDividers().get(0).positionProperty().addListener((obs, oldVal, newVal) -> {
            mapItemPane.setDividerPositions(newVal.doubleValue());
        });
        
	// AND PUT EVERYTHING IN THE WORKSPACE
	workspace = new BorderPane();
        app.getGUIModule().getTopToolbarPane().getChildren().add(polygonButtonsPane);
        app.getGUIModule().getTopToolbarPane().getChildren().add(imageButtonsPane);
        app.getGUIModule().getTopToolbarPane().getChildren().add(toggleButtonsPane);
        app.getGUIModule().getTopToolbarPane().getChildren().add(borderStylePane);

	((BorderPane)workspace).setCenter(outerPane);
        
        /** BACKGROUND GRADIENT **/
        proportionalCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                            ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                            ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                            ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                            ((RadialGradient) ocean.getFill()).getRadius(), 
                                                            newValue, 
                                                            ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                            ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        
        focusAngleSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                RadialGradient gradient = new RadialGradient((double) newValue,
                                                             ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                             ((RadialGradient) ocean.getFill()).getRadius(), 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        
        focusDistanceSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                             (double) newValue, 
                                                             ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                             ((RadialGradient) ocean.getFill()).getRadius(), 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        
        centerXSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                             ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                             (double) newValue,
                                                             ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                             ((RadialGradient) ocean.getFill()).getRadius(), 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        centerYSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                             ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                             (double) newValue, 
                                                             ((RadialGradient) ocean.getFill()).getRadius(), 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        
        
        radiusSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                             ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                             (double) newValue, 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        cycleMethodComboBox.valueProperty().addListener(new ChangeListener<CycleMethod>() {
            @Override
            public void changed(ObservableValue<? extends CycleMethod> observable, CycleMethod oldValue, CycleMethod newValue) {
                RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                             ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                             ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                             ((RadialGradient) ocean.getFill()).getRadius(), 
                                                             ((RadialGradient) ocean.getFill()).isProportional(), 
                                                             newValue, 
                                                             ((RadialGradient) ocean.getFill()).getStops());
                ocean.setFill(gradient);
            }
        });
        stop0ColorPicker.setOnAction(e-> {
            RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                         ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                         ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                         ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                         ((RadialGradient) ocean.getFill()).getRadius(), 
                                                         ((RadialGradient) ocean.getFill()).isProportional(), 
                                                         ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                         new Stop(0, stop0ColorPicker.getValue()),
                                                         ((RadialGradient) ocean.getFill()).getStops().get(1));
            ocean.setFill(gradient);
        });
        stop1ColorPicker.setOnAction(e-> {
            RadialGradient gradient = new RadialGradient(((RadialGradient) ocean.getFill()).getFocusAngle(),
                                                         ((RadialGradient) ocean.getFill()).getFocusDistance(), 
                                                         ((RadialGradient) ocean.getFill()).getCenterX(), 
                                                         ((RadialGradient) ocean.getFill()).getCenterY(), 
                                                         ((RadialGradient) ocean.getFill()).getRadius(), 
                                                         ((RadialGradient) ocean.getFill()).isProportional(), 
                                                         ((RadialGradient) ocean.getFill()).getCycleMethod(), 
                                                         ((RadialGradient) ocean.getFill()).getStops().get(0),
                                                         new Stop(1, stop1ColorPicker.getValue()));
            ocean.setFill(gradient);
        });
        
        MapNavigationController mapNavigationController = new MapNavigationController((RegioVincoMapMakerApp)app);
        resetViewportButton.setOnAction(e->{
            mapNavigationController.processResetViewport();
        });
        fitToRegionButton.setOnAction(e->{
            mapNavigationController.processFitToRegion();
        });
        
        MapMakerController mapController = new MapMakerController(app);
        mapPane.setOnMousePressed(e->{
            mapController.processMapMousePress((int)e.getX(), (int)e.getY());
        });
        mapPane.setOnMouseReleased(e->{
            mapController.processMapMouseRelease((int)e.getX(), (int)e.getY());
        });
//        mapPane.setOnMouseClicked(e->{
//            if ((e.getButton() == MouseButton.PRIMARY) && (e.getClickCount() == 2))
//                mapController.processMapMouseClicked(true, (int)e.getX(), (int)e.getY());
//            else if (e.getButton() == MouseButton.SECONDARY)
//                mapController.processMapMouseClicked(false, (int)e.getX(), (int)e.getY());
//        });
        mapPane.setOnScroll(e->{
            boolean zoomIn = e.getDeltaY() > 0;
            mapController.processMapMouseScroll(zoomIn, (int)e.getX(), (int)e.getY());
        });
        mapPane.setOnMouseMoved(e->{
            mapController.processMapMouseMoved((int)e.getX(), (int)e.getY());
        });
        mapPane.setOnMouseDragged(e->{
            mapController.processMapMouseDragged((int)e.getX(), (int)e.getY());
        });
        
        
        changeMapNameButton.setOnAction(e->{
            itemsController.processRenameMap();
        });
        moveUpButton.setOnAction(e->{
            itemsController.processMoveItemUp();
            app.getFileModule().markAsEdited(true);
        });
        moveDownButton.setOnAction(e->{
            itemsController.processMoveItemDown();
            app.getFileModule().markAsEdited(true);
        });
        
        mapPane.setOnMouseClicked(e->{
            itemsController.processMapClicked(mapPane);
            app.getFileModule().markAsEdited(true);
        });
        
    }
    
    public void initImages() {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        
        /* move Image */
        data.getImageView().setOnMousePressed(e->{
            imageController.processImageMousePress((int)e.getX(), (int)e.getY());
        });
        data.getImageView().setOnMouseDragged(e->{
            imageController.processImageMouseDragged((int)e.getX(), (int)e.getY());
        });
        data.getImageView().setOnMouseReleased(e->{
            System.out.println(e.getX() + ", " + e.getY());
            imageController.processImageMouseRelease((int)e.getX(), (int)e.getY());
        });
        snapTopLeftButton.setOnAction(e-> {
            data.getImageView().setTranslateX(0);
            data.getImageView().setTranslateY(0);
        });
        snapBottomLeftButton.setOnAction(e-> {
            data.getImageView().setTranslateX(0);
            data.getImageView().setTranslateY(mapPane.getHeight() - data.getImage().getHeight());
        });
    }
        
    public void initItems() {
        RegioVincoData data = (RegioVincoData) app.getDataComponent();
        
        itemsTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 1) {
                itemsController.processHighlightPolyMatching(data.getSelectedItem());
            }
            else if (e.getClickCount() == 2) {
                itemsController.processEditItem();
                app.getFileModule().markAsEdited(true);
            }
            app.getFoolproofModule().updateAll();
        });
    }

    public void initFoolproofDesign() {
        AppGUIModule gui = app.getGUIModule();
        AppFoolproofModule foolproofSettings = app.getFoolproofModule();
        foolproofSettings.registerModeSettings(RVMM_FOOLPROOF_SETTINGS, new RegioVincoSelectionFoolproofDesign((RegioVincoMapMakerApp)app));
    }
    
    @Override
    public void processWorkspaceKeyEvent(KeyEvent ke) {
        System.out.println("WORKSPACE REPONSE TO " + ke.getCharacter());
    }
    
    String filePath;
    String fileName;
    File file;
    
    @Override
    public void showNewDialog() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        //String title = "Create New Map";
                
        Dialog dialog = new Dialog();
        dialog.initOwner(app.getGUIModule().getWindow());
        dialog.setTitle(props.getProperty(NEW_WORK_TITLE));
        dialog.initModality(Modality.WINDOW_MODAL);

        BorderPane newDialogPane = new BorderPane();
        
        // header
        Pane headerPane = new Pane();

        Label headerLabel = new Label("Create New Map");
        headerLabel.getStyleClass().add(CLASS_RVMM_DIALOG_HEADER);
//        headerPane.getStyleClass().add(CLASS_RVMM_PANE);
        headerPane.setBackground(new Background(new BackgroundFill(Color.rgb(201, 224, 205), CornerRadii.EMPTY, Insets.EMPTY)));
        headerPane.setPrefSize(330, 70);
//        headerLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
//        headerLabel.setTextAlignment(TextAlignment.CENTER);
//        headerLabel.setPadding(new Insets(23, 5, 5, 25));
        headerLabel.setAlignment(Pos.CENTER);
        headerPane.getChildren().add(headerLabel);
        
        GridPane gridPane = new GridPane();
        gridPane.getStyleClass().add(CLASS_RVMM_DIALOG_GRID);

        Label regionNameLabel = new Label("Region Name");
        regionNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        // top right bottom left
        regionNameLabel.setPadding(new Insets(20, 10, 10, 10));
        
        TextField regionNameTextField = new TextField();
        
        Button directoryChooserButton = new Button("Choose Parent Region Directory");
        Label directoryChooserText = new Label(DEFAULT_DIRECTORY_PROMPT);
        directoryChooserText.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        directoryChooserText.setPadding(new Insets(20, 10, 10, 10));

        Button fileChooserButton = new Button("Choose Data File");
        Label fileChooserText = new Label(DEFAULT_FILE_PROMPT);
        fileChooserText.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        fileChooserText.setPadding(new Insets(20, 10, 10, 10));
       
        Button okButton = new Button("Ok");
        
        //flowPane.getChildren().add(gridPane);
        
        newDialogPane.setTop(headerPane);
        newDialogPane.setCenter(gridPane);
        
        gridPane.add(regionNameLabel, 0, 0);
        gridPane.add(regionNameTextField, 1, 0);
        gridPane.add(directoryChooserButton, 0, 1);
        gridPane.add(directoryChooserText, 1, 1);
        gridPane.add(fileChooserButton, 0, 2);
        gridPane.add(fileChooserText, 1, 2);
        gridPane.add(okButton, 1, 3);
        
        dialog.getDialogPane().setContent(newDialogPane);
        
        dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
 
        directoryChooserButton.setOnAction(e->{
            filePath = AppDialogsFacade.showDirectoryDialog(app.getGUIModule().getWindow(), NEW_WORK_TITLE);
            directoryChooserText.setText(filePath);
            dialog.getDialogPane().getScene().getWindow().sizeToScene();
        });
        fileChooserButton.setOnAction(e->{
            file = AppDialogsFacade.showOpenDialog(app.getGUIModule().getWindow(), NEW_WORK_TITLE);
            fileName = file.getName();
            fileChooserText.setText(fileName);
        });

        okButton.setOnAction(e->{
            String newMapName = regionNameTextField.getText() + ".json";
            File workDir = new File(filePath, regionNameTextField.getText());
            workDir.mkdir();
            File work = new File(workDir, newMapName);
            try {
                Files.copy(file.toPath(), work.toPath(), StandardCopyOption.REPLACE_EXISTING);
                app.getFileModule().loadWork(work);
            } catch (IOException ex) {
                System.out.println("not loaded or not saved as an item");
            }
            dialog.hide();
        });
        dialog.showAndWait();
    }

}
