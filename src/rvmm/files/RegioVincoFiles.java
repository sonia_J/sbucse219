package rvmm.files;

import java.util.ArrayList;
import rvmm.data.RegioVincoData;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import properties_manager.PropertiesManager;
import static djf.AppPropertyType.*;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javax.imageio.ImageIO;
import javax.swing.text.html.HTML;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import rvmm.RegioVincoMapMakerApp;
import static rvmm.RegioVincoMapMakerPropertyType.*;
import rvmm.data.RegioVincoItemPrototype;


/**
 *
 * @author McKillaGorilla
 * @coauthor Hyejun Jeong
 */
public class RegioVincoFiles implements AppFileComponent {
    RegioVincoMapMakerApp app;
    static final String JSON_NUMBER_OF_SUBREGIONS = "NUMBER_OF_SUBREGIONS";
    static final String JSON_SUBREGIONS = "SUBREGIONS";
    static final String JSON_SUBREGION_INDEX = "SUBREGION_INDEX";
    static final String JSON_NUMBER_OF_SUBREGION_POLYGONS = "NUMBER_OF_SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGONS = "SUBREGION_POLYGONS";
    static final String JSON_SUBREGION_POLYGON = "SUBREGION_POLYGON";
    static final String JSON_POLYGON_POINTS = "VERTICES";
    static final String JSON_POLYGON_POINT_X = "X";
    static final String JSON_POLYGON_POINT_Y = "Y";
    
    static final String JSON_ITEMS = "items";
    static final String JSON_SUBREGION = "subregion";
    static final String JSON_CAPITAL = "capital";
    static final String JSON_LEADER = "leader";
    
    // FOR EXPORTING TO HTML
    static final String TITLE_TAG = "title";
    static final String NAVIGATION_SETTING_TAG = "navigation_setting_mv";
    static final String SUBREGION_TAG = "subregion_rvmm";
    static final String TABLE_DATA_TAG = "regio_vinco_map_maker_table_data";
    
    public RegioVincoFiles(RegioVincoMapMakerApp initApp) {
        app = initApp;
    }
    
    /**
     * This method is for saving user work.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	RegioVincoData mapViewerData = (RegioVincoData)data;
        
	// NOW BUILD THE JSON ARRAY FOR THE LIST
	JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject mapMakerDataJSON = Json.createObjectBuilder()
		// CURRENTLY IT SAVES NOTHING
		.build();
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(mapMakerDataJSON);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(mapMakerDataJSON);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT WE ARE USING THE SIZE OF THE MAP
        RegioVincoData mapData = (RegioVincoData)data;
        mapData.reset();
        
        // LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	
        // THIS IS THE TOTAL NUMBER OF SUBREGIONS, EACH WITH
        // SOME NUMBER OF POLYGONS
        int numSubregions = getDataAsInt(json, JSON_NUMBER_OF_SUBREGIONS);
        JsonArray jsonSubregionsArray = json.getJsonArray(JSON_SUBREGIONS);

        // GO THROUGH ALL THE SUBREGIONS
        for (int subregionIndex = 0; subregionIndex < numSubregions; subregionIndex++) {
            // MAKE A POLYGON LIST FOR THIS SUBREGION
            JsonObject jsonSubregion = jsonSubregionsArray.getJsonObject(subregionIndex);
            int numSubregionPolygons = getDataAsInt(jsonSubregion, JSON_NUMBER_OF_SUBREGION_POLYGONS);
            ArrayList<ArrayList<Double>> subregionPolygonPoints = new ArrayList();
            // GO THROUGH ALL OF THIS SUBREGION'S POLYGONS
            for (int polygonIndex = 0; polygonIndex < numSubregionPolygons; polygonIndex++) {
                // GET EACH POLYGON (IN LONG/LAT GEOGRAPHIC COORDINATES)
                JsonArray jsonPolygon = jsonSubregion.getJsonArray(JSON_SUBREGION_POLYGONS);
                JsonArray pointsArray = jsonPolygon.getJsonArray(polygonIndex);
                ArrayList<Double> polygonPointsList = new ArrayList();
                for (int pointIndex = 0; pointIndex < pointsArray.size(); pointIndex++) {
                    JsonObject point = pointsArray.getJsonObject(pointIndex);
                    double pointX = point.getJsonNumber(JSON_POLYGON_POINT_X).doubleValue();
                    double pointY = point.getJsonNumber(JSON_POLYGON_POINT_Y).doubleValue();
                    polygonPointsList.add(pointX);
                    polygonPointsList.add(pointY);
                }
                subregionPolygonPoints.add(polygonPointsList);
            }
            mapData.addSubregion(subregionPolygonPoints);
            
        }
    }
    
    public double getDataAsDouble(JsonObject json, String dataName) {
	JsonValue value = json.get(dataName);
	JsonNumber number = (JsonNumber)value;
	return number.bigDecimalValue().doubleValue();	
    }
    
    public int getDataAsInt(JsonObject json, String dataName) {
        JsonValue value = json.get(dataName);
        JsonNumber number = (JsonNumber)value;
        return number.bigIntegerValue().intValue();
    }
    
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
   
    @Override
    public void exportData(AppDataComponent data, String savedFileName) throws IOException {
        String mapMakerName = savedFileName.substring(0, savedFileName.indexOf("."));
        String fileToExport = mapMakerName + ".html";
        try {
            RegioVincoData regioVincoData = (RegioVincoData) data;
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            String exportDirPath = props.getProperty(APP_PATH_EXPORT) + mapMakerName + "/";
            
            File exportDir = new File(exportDirPath);
            if(!exportDir.exists()) {
                exportDir.mkdir();
            }

            // LOAD THE TEMPLATE DOCUMENT
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            String htmlTemplatePath = props.getPropertiesDataPath() + props.getProperty(RVMM_EXPORT_TEMPLATE_FILE_NAME);
            File file = new File(htmlTemplatePath);
            System.out.println(file.getPath() + " exists? " + file.exists());
            URL templateURL = file.toURI().toURL();
            Document exportDoc = docBuilder.parse(templateURL.getPath());

            // SET THE WEB PAGE TITLE
            Node titleNode = exportDoc.getElementsByTagName(TITLE_TAG).item(0);
            titleNode.setTextContent(regioVincoData.getSubregionName());

            // SET THE NAME
            Node nameNode = getNodeWithId(exportDoc, HTML.Tag.TD.toString(), SUBREGION_TAG);
            nameNode.setTextContent(regioVincoData.getSubregionName());

            // ADD ALL THE ITEMS
            Node tDataNode = getNodeWithId(exportDoc, "tdata", TABLE_DATA_TAG);
            Iterator<RegioVincoItemPrototype> itemsItr = regioVincoData.itemsIterator();
            while (itemsItr.hasNext()) {
                RegioVincoItemPrototype item = itemsItr.next();
                Element trElement = exportDoc.createElement(HTML.Tag.TR.toString());
                tDataNode.appendChild(trElement);
                
                addCellToRow(exportDoc, trElement, item.getSubregion());
                addCellToRow(exportDoc, trElement, item.getCapital());
                addCellToRow(exportDoc, trElement, item.getLeader());
            }
            
        // CORRECT THE APP EXPORT PAGE
            props.addProperty(APP_EXPORT_PAGE, exportDirPath + fileToExport);

            // EXPORT THE WEB PAGE
            saveDocument(exportDoc, props.getProperty(APP_EXPORT_PAGE));
            takeSnapShot(app.getGUIModule().getWindow().getScene());
        } catch(SAXException | ParserConfigurationException | TransformerException exc) {
            throw new IOException("Error loading " + fileToExport);
        }
        
    }
    
    public void takeSnapShot(Scene scene) {
        WritableImage snapshot = new WritableImage((int)scene.getWidth(), (int)scene.getHeight());
        scene.snapshot(snapshot);
        File file = new File("snapshot.png");
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(snapshot, null), "png", file);
            System.out.println("Snapshot Saved: " + file.getAbsolutePath());
        } catch (IOException ex) {
            Logger.getLogger(RegioVincoFiles.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void addCellToRow(Document doc, Node rowNode, String text) {
        Element tdElement = doc.createElement(HTML.Tag.TD.toString());
        tdElement.setTextContent(text);
        rowNode.appendChild(tdElement);
    }
    private Node getNodeWithId(Document doc, String tagType, String searchID) {
        NodeList testNodes = doc.getElementsByTagName(tagType);
        for (int i = 0; i < testNodes.getLength(); i++) {
            Node testNode = testNodes.item(i);

            Node testAttr = testNode.getAttributes().getNamedItem(HTML.Attribute.ID.toString());
            if ((testAttr != null) && testAttr.getNodeValue().equals(searchID)) {
                return testNode;
            }
        }
        return null;
    }
    
    
    private void saveDocument(Document doc, String outputFilePath)
            throws TransformerException, TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Result result = new StreamResult(new File(outputFilePath));
        Source source = new DOMSource(doc);
        transformer.transform(source, result);
    }
    
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        
    }
}
