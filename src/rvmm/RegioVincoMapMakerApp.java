/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm;

import djf.AppTemplate;
import djf.components.AppClipboardComponent;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import djf.components.AppWorkspaceComponent;
import java.util.Locale;
import rvmm.clipboard.RegioVincoClipboard;
import rvmm.data.RegioVincoData;
import rvmm.files.RegioVincoFiles;
import rvmm.workspace.RegioVincoWorkspace;

/**
 *
 * @author HyejunJEONG
 */
public class RegioVincoMapMakerApp extends AppTemplate{
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);
        launch(args);
    }

    @Override
    public AppClipboardComponent buildClipboardComponent(AppTemplate app) {
        return new RegioVincoClipboard(this);
    }

    @Override
    public AppDataComponent buildDataComponent(AppTemplate app) {
        return new RegioVincoData(this);
    }

    @Override
    public AppFileComponent buildFileComponent() {
        return new RegioVincoFiles(this);
    }

    @Override
    public AppWorkspaceComponent buildWorkspaceComponent(AppTemplate app) {
        return new RegioVincoWorkspace(this);
    }
}
