/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.transactions;

import javafx.scene.image.ImageView;
import jtps.jTPS_Transaction;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class MoveImageTransaction implements jTPS_Transaction{
    RegioVincoData data;
    double oldX, oldY;
    double newX, newY;
    
    public MoveImageTransaction(RegioVincoData initData, double initOldX, double initOldY,
                                                         double initNewX, double initNewY) {
        data = initData;
        oldX = initOldX;
        oldY = initOldY;
        newX = initNewX;
        newY = initNewY;
    }
    
    @Override
    public void doTransaction() {
        ImageView img = data.getImageView();
        img.setTranslateX(newX);
        img.setTranslateY(newY);
    }

    @Override
    public void undoTransaction() {
        ImageView img = data.getImageView();
        img.setTranslateX(oldX);
        img.setTranslateY(oldY);
    }
    
}
