/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.transactions;

import jtps.jTPS_Transaction;
import rvmm.data.RegioVincoItemPrototype;

/**
 *
 * @author HyejunJEONG
 */
public class EditItemTransaction implements jTPS_Transaction {
    RegioVincoItemPrototype itemToEdit;
    String oldSubregionName, newSubregionName;
    String oldCapital, newCapital;
    String oldLeaderName, newLeaderName;

    public EditItemTransaction(    RegioVincoItemPrototype initItemToEdit, 
                                    String subregionName, 
                                    String capital, 
                                    String leaderName) {
        itemToEdit = initItemToEdit;
        oldSubregionName = itemToEdit.getSubregion();
        oldCapital = itemToEdit.getCapital();
        oldLeaderName = itemToEdit.getLeader();
        newSubregionName = subregionName;
        newCapital = capital;
        newLeaderName = leaderName;
    }

    @Override
    public void doTransaction() {
        itemToEdit.setSubregion(newSubregionName);
        itemToEdit.setCapital(newCapital);
        itemToEdit.setLeader(newLeaderName);
    }

    @Override
    public void undoTransaction() {
        itemToEdit.setSubregion(oldSubregionName);
        itemToEdit.setCapital(oldCapital);
        itemToEdit.setLeader(oldLeaderName);
    }

}
