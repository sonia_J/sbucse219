/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.transactions;

import jtps.jTPS_Transaction;
import rvmm.data.RegioVincoData;
import rvmm.data.RegioVincoItemPrototype;

/**
 *
 * @author HyejunJEONG
 */
public class AddItemTransaction implements jTPS_Transaction {
    RegioVincoData data;
    RegioVincoItemPrototype itemToAdd;
    double x;
    double y;
    
    public AddItemTransaction(RegioVincoData initData, RegioVincoItemPrototype initNewItem) {
        data = initData;
        itemToAdd = initNewItem;
        
    }
    
    @Override
    public void doTransaction() {
        data.addItem(itemToAdd, x, y);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
    }
    
}
