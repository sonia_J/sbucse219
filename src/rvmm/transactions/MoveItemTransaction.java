/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.transactions;

import jtps.jTPS_Transaction;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class MoveItemTransaction implements jTPS_Transaction{
    RegioVincoData data;
    int oldIndex;
    int newIndex;
    
    public MoveItemTransaction(RegioVincoData initData, int initOldIndex, int initNewIndex) {
        data = initData;
        oldIndex = initOldIndex;
        newIndex = initNewIndex;
    }
    
    @Override
    public void doTransaction() {
        data.moveItem(oldIndex, newIndex);
    }

    @Override
    public void undoTransaction() {
        data.moveItem(newIndex, oldIndex);
    }   
}