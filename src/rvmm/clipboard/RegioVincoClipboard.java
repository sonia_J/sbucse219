/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.clipboard;

import djf.components.AppClipboardComponent;
import rvmm.RegioVincoMapMakerApp;
import rvmm.data.RegioVincoData;

/**
 *
 * @author HyejunJEONG
 */
public class RegioVincoClipboard implements AppClipboardComponent{
    RegioVincoMapMakerApp app;

    public RegioVincoClipboard(RegioVincoMapMakerApp initApp) {
        app = initApp;
    }
    
    @Override
    public void cut() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
    }

    @Override
    public void copy() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
    }

    @Override
    public void paste() {
        RegioVincoData data = (RegioVincoData)app.getDataComponent();
    }

    @Override
    public boolean hasSomethingToCut() {
        return false;
    }

    @Override
    public boolean hasSomethingToCopy() {
        return false;
    }

    @Override
    public boolean hasSomethingToPaste() {
        return false;
    }
}
