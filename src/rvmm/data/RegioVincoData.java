package rvmm.data;

import static djf.AppPropertyType.*;
import djf.components.AppDataComponent;
import djf.modules.AppFileModule;
import djf.modules.AppGUIModule;
import djf.ui.dialogs.AppDialogsFacade;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import rvmm.RegioVincoMapMakerApp;
import javafx.scene.control.TableView.TableViewSelectionModel;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import static rvmm.data.RegioVincoData.State.MAP_DRAG;
import static rvmm.data.RegioVincoData.State.MAP_SELECT;
import static rvmm.RegioVincoMapMakerPropertyType.*;
import static rvmm.data.RegioVincoData.State.IMAGE_DRAG;
import static rvmm.data.RegioVincoData.State.IMAGE_SELECT;
import rvmm.workspace.RegioVincoWorkspace;
import rvmm.workspace.controllers.ItemsController;
import static rvmm.workspace.style.RVMMStyle.*;


/**
 *
 * @author McKillaGorilla
 * @co-author Hyejun Jeong
 */
public class RegioVincoData implements AppDataComponent {
    // THE APP ITSELF
    RegioVincoMapMakerApp app;
    ObservableList<RegioVincoItemPrototype> items;
    TableViewSelectionModel itemsSelectionModel;
    // tried to link polygon, which we are gonna double click,
    // and itemPrototype
    ObjectProperty<Polygon> polygonProperty;
    StringProperty subregionNameProperty;
    StringProperty capitalProperty;
    StringProperty leaderProperty;

    // THE PANE WHERE WE'RE PUTTING ALL THE POLYGONS
    SplitPane splitPane;
    Pane map;
    int subregionId;
    HashMap<Integer, ObservableList<Polygon>> subregions;
    HashMap<Polygon, RegioVincoItemPrototype> polyItem;

    State currentState;
    final double DEFAULT_LINE_THICKNESS = 1.0;
    private boolean mouseDragOnDivider = false;
    
    RegioVincoWorkspace workspace;

    enum State{
        MAP_SELECT, MAP_DRAG,
        IMAGE_SELECT, IMAGE_DRAG
    }
    public RegioVincoData(RegioVincoMapMakerApp initApp) {
        app = initApp;
        subregions = new HashMap();
        polyItem = new HashMap();
        map = (Pane)app.getGUIModule().getGUINode(RVMM_MAP_PANE);
        workspace = (RegioVincoWorkspace) app.getWorkspaceComponent();
        // ITEMS TABLE
        TableView tableView = (TableView) app.getGUIModule().getGUINode(RVMM_ITEMS_TABLE_VIEW);
        //tableView.setItems(items);
        items = tableView.getItems();
        itemsSelectionModel = tableView.getSelectionModel();
        itemsSelectionModel.setSelectionMode(SelectionMode.MULTIPLE);
        
        currentState = State.MAP_SELECT;
    }
    
    public Pane getMapPane() {
        return map;
    }
    
    
    public void fitToRegion() {
        // GO THROUGH ALL THE POLYGONS TO FIND THE BOUNDS
        double minX = 5000;
        double minY = 5000;
        double maxX = -5000;
        double maxY = -5000;
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);            
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                Bounds polyBounds = poly.getBoundsInLocal();
                if (polyBounds.getMinX() < minX)
                    minX = polyBounds.getMinX();
                if (polyBounds.getMaxX() > maxX)
                    maxX = polyBounds.getMaxX();
                if (polyBounds.getMinY() < minY)
                    minY = polyBounds.getMinY();
                if (polyBounds.getMaxY() > maxY)
                    maxY = polyBounds.getMaxY();
            }
        }
        double BUFFER_PERCENT = 0.05;
        minX -= (minX * BUFFER_PERCENT);
        maxX += (maxX * BUFFER_PERCENT);
        minY -= (minY * BUFFER_PERCENT);
        maxY += (maxY * BUFFER_PERCENT);
      
        // CALCULATE THE CENTER
        double centerX = (minX + maxX)/2.0;
        double centerY = (minY + maxY)/2.0f;
        double mapWidth = map.widthProperty().doubleValue();
        double mapHeight = map.heightProperty().doubleValue();
        double scaleX = mapWidth/(maxX - minX);
        double scaleY = mapHeight/(maxY - minY);
        double scale = scaleX;
        if (scaleY > scale) {
            scale = scaleY;
        }
        double factor = scale/map.scaleXProperty().doubleValue();
        
        // FIRST MOVE THE MAP SO IT'S CENTERED ON THE CENTER
        double translateX = (mapWidth/2) - centerX;
        double translateY = (mapHeight/2) - centerY;
        this.resetViewport();
        map.translateXProperty().setValue(translateX);
        map.translateYProperty().setValue(translateY);
        this.zoomOnPoint(factor, centerX, centerY);
    }
    
    public Polygon getMousedOverPolygon(double x, double y) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);            
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                double localX = x - poly.getLayoutX();
                double localY = y - poly.getLayoutY();
                if (poly.contains(x, y)) {
                    return poly;
                }
            }
        }
        return null;
    }
    
    public double calcXPerc(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftX = (xMax - translateX)/scale;
        double percentX = (x-leftX)/(map.getWidth()/scale);
        return percentX;        
    }
    
    public double calcYPerc(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double yMax = (scale-1.0)*(map.getWidth()/2.0);
        double leftY = (yMax - translateY)/scale;
        double percentY = (y-leftY)/(map.getHeight()/scale);
        return percentY;        
    }
    
    public ObservableList<Polygon> getSubregion(int id) {
        return subregions.get(id);
    }
    
    @Override
    public void reset() {
        AppGUIModule gui = app.getGUIModule();

        // CLEAR THE DATA
        subregions.clear();
        
        subregionId = 0;
        
        // AND THE POLYGONS THEMSELVES
        Rectangle ocean = (Rectangle)map.getChildren().get(0);
        map.getChildren().clear();
        map.getChildren().add(ocean);
        
        // CLEAR OUT THE ITEMS FROM THE TABLE
        TableView tableView = (TableView)gui.getGUINode(RVMM_ITEMS_TABLE_VIEW);
        items = tableView.getItems();
        items.clear();
    }
    
    public void addSubregion(ArrayList<ArrayList<Double>> rawPolygons) {
        ObservableList<Polygon> subregionPolygons = FXCollections.observableArrayList();
        for (int i = 0; i < rawPolygons.size(); i++) {
            ArrayList<Double> rawPolygonPoints = rawPolygons.get(i);
            Polygon polygonToAdd = new Polygon();
            ObservableList<Double> transformedPolygonPoints = polygonToAdd.getPoints();
            for (int j = 0; j < rawPolygonPoints.size(); j+=2) {
                double longX = rawPolygonPoints.get(j);
                double latY = rawPolygonPoints.get(j+1);
                double x = longToX(longX);
                double y = latToY(latY);
                transformedPolygonPoints.addAll(x, y);
            }
            subregionPolygons.add(polygonToAdd);
            double randomNum = Math.random();
            polygonToAdd.setFill(Color.gray(randomNum));
            //polygonToAdd.getStyleClass().add(CLASS_RVMM_MAP_LAND);
            polygonToAdd.setStroke(Color.BLACK);
            polygonToAdd.setStrokeWidth(DEFAULT_LINE_THICKNESS);
            polygonToAdd.setUserData(subregionId);
            map.getChildren().add(polygonToAdd);
        }
        subregions.put(subregionId, subregionPolygons);
        subregionId++;   
    }
    
    public void reassignColor() {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                double randomNum = Math.random();
                poly.setFill(Color.gray(randomNum));
            }
        }
    }
    
    ItemsController itemsController;

    public Polygon getPolygon(RegioVincoItemPrototype selectedItem) {
        itemsController = new ItemsController(app);
        for(Polygon poly : polyItem.keySet()) {
            if(polyItem.get(poly).equals(selectedItem)) {
                return poly;
            }
        }
        return null;
    }
    
    public RegioVincoItemPrototype getItem(Polygon poly) {
        itemsController = new ItemsController(app);
        //if(polyItem.containsKey(poly)) {
            for(RegioVincoItemPrototype item : polyItem.values()) {
                if(polyItem.get(poly).equals(item)) {
                    return item;
                }
            }
        //}
        return null;
    }
    
    public HashMap getPolyItemMap() {
        return polyItem;
    }
    
    /**
     * This calculates and returns the x pixel value that corresponds to the
     * xCoord longitude argument.
     */
    public double longToX(double longCoord) {
        double paneHeight = map.getHeight();
        double unitDegree = paneHeight/180;
        double newLongCoord = (longCoord + 180) * unitDegree;
        return newLongCoord;
    }

    /**
     * This calculates and returns the y pixel value that corresponds to the
     * yCoord latitude argument.
     */
    public double latToY(double latCoord) {
        // DEFAULT WILL SCALE TO THE HEIGHT OF THE MAP PANE
        double paneHeight = map.getHeight();
        
        // WE ONLY WANT POSITIVE COORDINATES, SO SHIFT BY 90
        double unitDegree = paneHeight/180;
        double newLatCoord = (latCoord + 90) * unitDegree;
        return paneHeight - newLatCoord;
    }

    double startX;
    double startY;
    
    public void resetLocation() {
        map.translateXProperty().set(0);
        map.translateYProperty().set(0);
    }    

    public void resetViewport() {
        scaleMap(1.0);
        moveMap(0, 0);
    }

    public void expandMapView() {
        splitPane.getDividers().get(0).positionProperty().addListener((obs, oldVal, newVal) -> {
            splitPane.setDividerPositions(newVal.doubleValue());
        });
        
        for(Node node: splitPane.lookupAll(".split-pane-divider")) {
            node.setOnMousePressed(evMousePressed -> mouseDragOnDivider = true);
            node.setOnMouseReleased(evMouseReleased -> mouseDragOnDivider = false );
        }
    }
    ImageView imageView;
    Image image;
    public void addImage() {
        FileChooser fileChooser = new FileChooser();
        imageView = new ImageView();
        
            //Set extension filter
        FileChooser.ExtensionFilter extFilterPNG = new FileChooser.ExtensionFilter("PNG files (*.png)", "*.PNG");
        FileChooser.ExtensionFilter extFilterJPG = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.JPG");

        fileChooser.getExtensionFilters().addAll(extFilterPNG, extFilterJPG);
             
            //Show open file dialog
        File file = fileChooser.showOpenDialog(null);
               
        ChangeListener<Number> listener = new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                Bounds boundsInScene = imageView.localToScene(imageView.getBoundsInLocal());
                double xInScene = boundsInScene.getMinX();
                double yInScene = boundsInScene.getMinY();
            }
        };
        try {
            BufferedImage bufferedImage = ImageIO.read(file);
            image = SwingFXUtils.toFXImage(bufferedImage, null);
            imageView.setImage(image);
            map.getChildren().add(imageView);
            workspace.initImages();
            imageView.translateXProperty().addListener(listener);
            imageView.translateYProperty().addListener(listener);

        } catch (Exception e) {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), ADD_IMAGE_ERROR_TITLE, ADD_IMAGE_ERROR_CONTENT);
        }
    }
    
    public void removeImage() {
        if(imageView != null) {
            map.getChildren().remove(imageView);
        }
        else {
            AppDialogsFacade.showMessageDialog(app.getGUIModule().getWindow(), REMOVE_IMAGE_ERROR_TITLE, REMOVE_IMAGE_ERROR_CONTENT);
        }
    }
    
    public ImageView getImageView() {
        return imageView;
    }
    
    public Image getImage() {
        return image;
    }
    
    public void renameMap() {
        String result = AppDialogsFacade.showTextInputDialog(app.getGUIModule().getWindow(), app, 
                        RVMM_RENAME_MAP_DIALOG_TITLE, RVMM_RENAME_MAP_DIALOG_HEADER_TEXT, RVMM_RENAME_MAP_DIALOG_CONTENT_TEXT);
        AppFileModule fileSettings = app.getFileModule();
        File currentFile = fileSettings.getWorkFile();
       
        currentFile.renameTo(new File(result));
    }
    
    public void resizeMap() {
        
    }
    
    private void scaleMap(double zoomScale) {
        map.scaleXProperty().setValue(zoomScale);
        map.scaleYProperty().setValue(zoomScale);
    }
    private void moveMap(double x, double y) {
        map.translateXProperty().set(x);
        map.translateYProperty().set(y);
    }

    public void zoom(double zoomInc) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        if ((scale * zoomInc) >= 1.0) {
            scale *= zoomInc;
            moveMap(0,0);
            scaleMap(scale);
            translateX *= zoomInc;
            translateY *= zoomInc;
            moveMap(translateX, translateY);
            clamp();
            adjustLineThickness();
        }
    }

    public void clamp() {
        // FIRST CLAMP X
        double scale = map.scaleXProperty().doubleValue();
        double xMax = (scale-1.0)*(map.getWidth()/2.0);
        double xTranslate = map.translateXProperty().doubleValue();
        if (xTranslate > xMax) xTranslate = xMax;
        //else if (xTranslate < adjustedX) xTranslate = adjustedX;
        map.translateXProperty().setValue(xTranslate);

        // THEN Y
        double yMax = (scale-1.0)*(map.getHeight()/2.0);
        double yTranslate = map.translateYProperty().doubleValue();
        if (yTranslate > yMax) yTranslate = yMax;
        map.translateYProperty().setValue(yTranslate);
    }
    
    public void adjustLineThickness() {
        double scale = map.scaleXProperty().doubleValue();
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStrokeWidth(DEFAULT_LINE_THICKNESS/scale);
            }
        }
    }
    
    public void toggleBorderFrame(Pane pane) {
        pane.setBorder(new Border(new BorderStroke(Color.BLACK, 
            BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.FULL)));
    }
    
    public void changeLineThickness(double strokeWidth) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStrokeWidth(strokeWidth);
            }
        }
    }
    
    public void changeLineColor(Color color) {
        Iterator<Integer> idIt = subregions.keySet().iterator();
        while (idIt.hasNext()) {
            int id = idIt.next();
            ObservableList<Polygon> polygons = subregions.get(id);
            Iterator<Polygon> polyIt = polygons.iterator();
            while (polyIt.hasNext()) {
                Polygon poly = polyIt.next();
                poly.setStroke(color);
            }
        }
    }

    public void move(double xInc, double yInc) {
        // FIRST X, WITH CLAMPING AT THE EDGES
        double xTranslate = map.translateXProperty().doubleValue() + xInc;
        map.translateXProperty().setValue(xTranslate);

        double yTranslate = map.translateYProperty().doubleValue() + yInc;
        map.translateYProperty().setValue(yTranslate);
        
        // MAKE SURE WE'RE NOT OUT OF BOUNDS
        clamp();
    }

    public double xToScaledX(double x) {
        double scale = map.scaleXProperty().doubleValue();
        double translateX = map.translateXProperty().doubleValue();
        double diffX = x - translateX;        
        return diffX/scale;
    }
    
    public double yToScaledY(double y) {
        double scale = map.scaleXProperty().doubleValue();
        double translateY = map.translateYProperty().doubleValue();
        double diffY = y - translateY;
        return diffY/scale;
    }
    
    public State getState() {
        return currentState;
    }
    
    /** Moving a map */
    public void startMapDrag(int x, int y) {
        if (currentState != IMAGE_DRAG && currentState != IMAGE_SELECT)  {
            startX = x;
            startY = y;
            currentState = MAP_DRAG;
            app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
        }
    }

    public void updateMapDrag(double x, double y) {
        if (currentState == MAP_DRAG) {
            double diffX = x - startX;
            double diffY = y - startY;
            this.move(diffX, diffY);
        }        
    }

    public void endMapDrag(int x, int y) {
        currentState = MAP_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
    }
    
    /** Moving an Image */

    private double startImageX;
    private double startImageY;
    
    public void startImageDrag(int x, int y) {
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.MOVE);
        startImageX = x;
        startImageY = y;
        currentState = IMAGE_DRAG;
    }

    public void updateImageDrag(double x, double y) {
        if(currentState == IMAGE_DRAG) {
            double diffX = x - startImageX;
            double diffY = y - startImageY;
            moveImage(diffX, diffY);
        }
    }
    
    private void moveImage(double xInc, double yInc) {
        double xTranslate = imageView.getTranslateX() + xInc;
        double yTranslate = imageView.getTranslateY() + yInc;
        imageView.setTranslateX(xTranslate);
        imageView.setTranslateY(yTranslate);
    }
    
    public void endImageDrag(int x, int y) {
        startImageX = x;
        startImageY = y;
        currentState = MAP_SELECT;
        app.getGUIModule().getWindow().getScene().setCursor(Cursor.DEFAULT);
    }

    public void zoomInOnPoint(double x, double y) {
        zoomOnPoint(2.0, x, y);
    }

    public void zoomOutOnPoint(double x, double y) {
        zoomOnPoint(0.5, x, y);
    }

    private void zoomOnPoint(double factor, double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        double newScale = factor * scale;
        if (newScale >= 1.0) {
            updateStats(mouseX, mouseY);
            double diffX =(viewportMousePercentX * (viewportWidth/newScale));
            double diffY = (viewportMousePercentY * (viewportHeight/newScale));
            double newWorldViewportX = worldMouseX - diffX;
            double newWorldViewportY = worldMouseY - diffY;
            double newWorldViewportPaddingLeft = (newScale-1.0)*(viewportWidth/2.0);
            double newWorldViewportPaddingTop = (newScale-1.0)*(viewportHeight/2.0);
            viewportTranslateX = newWorldViewportPaddingLeft - (newWorldViewportX * newScale);
            viewportTranslateY = newWorldViewportPaddingTop - (newWorldViewportY * newScale);

            zoom(factor);
            map.translateXProperty().setValue(viewportTranslateX);
            map.translateYProperty().setValue(viewportTranslateY);
            this.update(mouseX, mouseY);
        }
    }
    
    
    Polygon mousedOverPolygon;
    
//    public void highlightPolygon(double mouseX, double mouseY) {
//        Polygon poly = this.getMousedOverPolygon(mouseX, mouseY);
//        if (poly != null) {
//            if (mousedOverPolygon != null) {         
//                mousedOverPolygon.getStyleClass().remove(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
//                mousedOverPolygon.getStyleClass().add(CLASS_RVMM_MAP_LAND);
//            }
//            mousedOverPolygon = poly;
//            mousedOverPolygon.getStyleClass().remove(CLASS_RVMM_MAP_LAND);
//            mousedOverPolygon.getStyleClass().add(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
//        }
//        else if (mousedOverPolygon != null) { 
//            mousedOverPolygon.getStyleClass().remove(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
//            mousedOverPolygon.getStyleClass().add(CLASS_RVMM_MAP_LAND);
//            mousedOverPolygon = null;    
//        }
//    }
    Polygon highlightedPoly;
    public void highlightPolygon(Polygon poly) {
        if(getHighlightedPoly() == null) { // first time we clicked on the item
            if(poly != null) {
                poly.getStyleClass().remove(CLASS_RVMM_MAP_LAND);
                poly.getStyleClass().add(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
                highlightedPoly = poly;
            }
        }
        else { // gethighlightedpoly != null
            highlightedPoly = getHighlightedPoly();
            highlightedPoly.getStyleClass().remove(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
            highlightedPoly.getStyleClass().add(CLASS_RVMM_MAP_LAND);
            poly.getStyleClass().remove(CLASS_RVMM_MAP_LAND);
            poly.getStyleClass().add(CLASS_RVMM_MAP_MOUSE_OVER_LAND);
            highlightedPoly = poly;
        } 
    }
    
    RegioVincoItemPrototype highlightedItem;
    public void highlightItem(RegioVincoItemPrototype item) {
        if(getHighlightedItem() == null) {
            if(item != null) {
                selectItem(item);
                highlightedItem = item;
            }
        }
        else {//if(getHighlightedItem() != null) {
            highlightedItem = getHighlightedItem();
            this.itemsSelectionModel.clearSelection();
            System.out.println("highlighteditem != null");
            selectItem(item);
            highlightedItem = item;
        }
        workspace.initItems();

    }
    
    public RegioVincoItemPrototype getHighlightedItem() {
        return highlightedItem;
    }
    
    
    public Polygon getHighlightedPoly() {
        return highlightedPoly;
    }
    
    
    public void update(double mouseX, double mouseY) {
        updateStats(mouseX, mouseY);
//        highlightPolygon(mouseX, mouseY);
        //displayStats();
    }
    
    public String getSubregionName() {
        return subregionNameProperty.getValue();
    }
    
    public boolean isValidToDoItemEdit(RegioVincoItemPrototype itemToEdit, String subregionName, String capital, String leaderName) {
        return isValidNewToDoItem(subregionName, capital, leaderName);
    }
    
    public boolean isValidNewToDoItem(String subregionName, String capital, String leaderName) {
        if (subregionName.trim().length() == 0)
            return false;
        if (capital.trim().length() == 0)
            return false;
        if (leaderName.trim().length() == 0)
            return false;
        
        return true;
    }
    
    public Iterator<RegioVincoItemPrototype> itemsIterator() {
        return this.items.iterator();
    }
    
    public void selectItem(RegioVincoItemPrototype itemToSelect) {
        this.itemsSelectionModel.select(itemToSelect);
    }
    
    public boolean isItemSelected() {
        ObservableList<RegioVincoItemPrototype> selectedItems = this.getSelectedItems();
        return (selectedItems != null) && (selectedItems.size() == 1);
    }
    
    public RegioVincoItemPrototype getSelectedItem() {
        if (!isItemSelected()) {
            return null;
        }
        return getSelectedItems().get(0);
    }
    
    public ObservableList<RegioVincoItemPrototype> getSelectedItems() {
        return (ObservableList<RegioVincoItemPrototype>)this.itemsSelectionModel.getSelectedItems();
    }
    
    public ObservableList<RegioVincoItemPrototype> getItems() {
        return items;
    }

    public int getItemIndex(RegioVincoItemPrototype item) {
        return items.indexOf(item);
    }
    
    public RegioVincoItemPrototype getItemAt(int itemIndex) {
        return items.get(itemIndex);
    }    
    public Boolean doesHashMapContainPoly(double x, double y) {
        return polyItem.containsKey(getMousedOverPolygon(x, y));
    }
    Polygon poly;
    public void addItem(RegioVincoItemPrototype itemToAdd, double x, double y) {
        poly = getMousedOverPolygon(x, y);
        items.add(itemToAdd);
        //polyItem.put(getMousedOverPolygon(x, y), itemToAdd);
        polyItem.put(poly, itemToAdd);
        workspace.initItems();
    }   
    
    public void removeItem(RegioVincoItemPrototype itemToAdd) {
        items.remove(itemToAdd);
        polyItem.remove(poly, itemToAdd);
        workspace.initItems();
    }
    
    public void moveItem(int oldIndex, int newIndex) {
        RegioVincoItemPrototype itemToMove = items.remove(oldIndex);
        items.add(newIndex, itemToMove);
    }
    
    public int getNumItems() {
        return items.size();
    }

    public ArrayList<RegioVincoItemPrototype> getCurrentItemsOrder() {
        ArrayList<RegioVincoItemPrototype> orderedItems = new ArrayList();
        for (RegioVincoItemPrototype item : items) {
            orderedItems.add(item);
        }
        return orderedItems;
    }

    public void clearSelected() {
        this.itemsSelectionModel.clearSelection();
    }
    
    public void rearrangeItems(ArrayList<RegioVincoItemPrototype> oldListOrder) {
        items.clear();
        for (RegioVincoItemPrototype item : oldListOrder) {
            items.add(item);
        }
    }
    
    // VIEWPORT
    double viewportWidth, viewportHeight;
    double viewportTranslateX, viewportTranslateY;
    double viewportMousePercentX, viewportMousePercentY;
    
    // THESE ARE VALUES USING WORLD COORDINATES
    double worldWidth, worldHeight;
    double worldMouseX, worldMouseY;
    double worldViewportWidth, worldViewportHeight;
    double worldViewportPaddingLeft, worldViewportPaddingTop;
    double worldViewportX, worldViewportY;

    private void updateStats(double mouseX, double mouseY) {
        double scale = map.scaleXProperty().doubleValue();
        viewportWidth = map.widthProperty().doubleValue();
        viewportHeight = map.heightProperty().doubleValue();
        viewportTranslateX = map.translateXProperty().doubleValue();
        viewportTranslateY = map.translateYProperty().doubleValue();
        
        worldWidth = map.heightProperty().doubleValue()*2.0;
        worldHeight = map.heightProperty().doubleValue();
        worldMouseX = mouseX;
        worldMouseY = mouseY;
        worldViewportWidth = viewportWidth/scale;
        worldViewportHeight = viewportHeight/scale;
        worldViewportPaddingLeft = (scale-1.0)*(map.getWidth()/2.0);
        worldViewportPaddingTop = (scale-1.0)*(map.getHeight()/2.0);
        worldViewportX = (worldViewportPaddingLeft - viewportTranslateX)/scale;
        worldViewportY = (worldViewportPaddingTop - viewportTranslateY)/scale;
        
        viewportMousePercentX = (worldMouseX - worldViewportX)/worldViewportWidth;
        viewportMousePercentY = (worldMouseY - worldViewportY)/worldViewportHeight;
    }
}