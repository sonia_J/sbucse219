/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rvmm.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 *
 * @author HyejunJEONG
 */
public class RegioVincoItemPrototype implements Cloneable {
    public static final String DEFAULT_SUBREGION = "Nowhere";
    public static final String DEFAULT_CAPITAL = "Nowhere";
    public static final String DEFAULT_LEADER = "No one";
    
    final StringProperty subregion;
    final StringProperty capital;
    final StringProperty leader;
    
    
    public RegioVincoItemPrototype() {
        subregion = new SimpleStringProperty(DEFAULT_SUBREGION);
        capital = new SimpleStringProperty(DEFAULT_CAPITAL);
        leader = new SimpleStringProperty(DEFAULT_LEADER);
    }
    
    public RegioVincoItemPrototype(String initSubregion, String initCapital, String initLeader) {
        this();
        subregion.set(initSubregion);
        capital.set(initCapital);
        leader.set(initLeader);
    } 
    
    public String getSubregion() {
        return subregion.get();
    }
    
    public void setSubregion(String value) {
        subregion.set(value);
    }
    
    public StringProperty subregionProperty() {
        return subregion;
    }
    
    public String getCapital() {
        return capital.get();
    }
    
    public void setCapital(String value) {
        capital.set(value);
    }
    
    public StringProperty capitalProperty() {
        return capital;
    }
    
    public String getLeader() {
        return leader.get();
    }

    public void setLeader(String value) {
        leader.set(value);
    }
    
    public StringProperty leaderProperty() {
        return leader;
    }
    
    public void reset() {
        setSubregion(DEFAULT_SUBREGION);
        setCapital(DEFAULT_CAPITAL);
        setLeader(DEFAULT_LEADER);
    }
    
    public Object clone() {
        return new RegioVincoItemPrototype(subregion.getValue(), 
                                        capital.getValue(), 
                                        leader.getValue());
    }
    
    public boolean equals(Object obj) {
        return this == obj;
    }
    
    @Override
    public String toString() {
        String description = this.getSubregion() + ": " + 
                             this.getCapital() + ", " +
                             this.getLeader();
        return description;
    }
    
}
    

